package com.javarush.test.level26.lesson02.task01;

/* Почитать в инете про медиану выборки
Реализовать логику метода sort, который должен сортировать данные в массиве по удаленности от его медианы
Вернуть отсортированный массив от минимального расстояния до максимального
Если удаленность одинаковая у нескольких чисел, то выводить их в порядке возрастания
*/


import java.lang.reflect.Array;
import java.util.*;


public class Solution {

    public static void main(String[] args)
    {
        Integer[] n = {1,2,3,4,5,6,7,8,9,10};
        Integer[] n2 = {1,2,3,4,5,6,7,8,9};

        System.out.println(Arrays.toString(sort(n)));
        System.out.println(Arrays.toString(sort(n2)));
    }

    public static Integer[] sort(Integer[] array)
    {
        //implement logic here

        final double mediana;
        Arrays.sort(array);
        if((array.length%2)==0) mediana = (array[array.length/2-1]+array[array.length/2])/2D;
        else mediana = array[array.length/2];

        Comparator<Integer> med = new Comparator<Integer>()
        {
            @Override
            public int compare(Integer o1, Integer o2)
            {
                int c = (int) (Math.abs(o1-mediana)-Math.abs(o2-mediana));
                if(c==0) c = o1-o2;

                return c;
            }
        };

        Arrays.sort( array , med);



        System.out.println(mediana);

        // array = list.toArray(array);

        return array;
    }
}





//public class Solution {
//    public static Integer[] sort(Integer[] array) {
//        //implement logic here
//        return array;
//    }
//}

