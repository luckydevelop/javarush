package com.javarush.test.level26.lesson15.big01;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lucky on 01.05.2015.
 */
public class CurrencyManipulator
{
    String currencyCode;

    public CurrencyManipulator(String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    Map<Integer, Integer> denominations = new HashMap<Integer, Integer>();



    public void addAmount(int denomination, int count){
        if(denominations.containsKey(denomination))
            denominations.put(denomination, denominations.get(denomination) + count);
        else
            denominations.put(denomination,count);
    }

    public int getTotalAmount()
    {
        int sum=0;

        for (Map.Entry<Integer, Integer> pair : denominations.entrySet())
        {
            sum +=pair.getKey()*pair.getValue();
        }

        return sum;
    }

    public boolean hasMoney()
    {
        return !denominations.isEmpty();
    }

//    3. Запустим прогу и сразу первой операцией попросим INFO. Ничего не вывело? Непорядок.
//    Добавьте в манипулятор метод boolean hasMoney(), который будет показывать, добавлены ли какие-то банкноты или нет.
}
