package com.javarush.test.level26.lesson15.big01;

import com.javarush.test.level26.lesson15.big01.command.CommandExecutor;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Lucky on 03.04.2015.
 */
public class CashMachine
{
    public static void main(String[] args) throws IOException
    {
//        1. Перенесите логику из main в DepositCommand и InfoCommand
//        Проверим, что там стало с main? Цикл, в котором спрашиваем операцию у пользователя,
//            а потом вызываем метод у CommandExecutor.
//            И так до бесконечности... надо бы придумать условие выхода из цикла.
//            Исправь цикл, чтоб он стал do-while. Условие выхода - операция EXIT.


        Locale.setDefault(Locale.ENGLISH);
        Operation answer;

        try
        {

            do
            {
                answer = ConsoleHelper.askOperation();

                CommandExecutor.execute(answer);
            }
            while (answer != Operation.EXIT);

        }

        catch (InterruptOperationException e)
        {
            try
            {
                CommandExecutor.execute(Operation.EXIT);
            }
            catch (InterruptOperationException e1)
            {

            }

            ConsoleHelper.printExitMessage();

        }






    }
}


