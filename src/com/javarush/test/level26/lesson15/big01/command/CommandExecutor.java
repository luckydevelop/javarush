package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.Operation;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lucky on 06.05.2015.
 */
public class CommandExecutor
{
   public static Map<Operation, Command> map = new HashMap<Operation, Command>();

    static
    {
        map.put(Operation.INFO, new InfoCommand());
        map.put(Operation.DEPOSIT, new DepositCommand());
        map.put(Operation.WITHDRAW, new WithdrawCommand());
        map.put(Operation.EXIT, new ExitCommand());
    }
    CommandExecutor()
    {

    }

    public static final void execute(Operation operation) throws IOException, InterruptOperationException
    {
        map.get(operation).execute();
    }

}


//4.1 Создадим метод public static final void execute(Operation operation), который будет дергать
//        метод execute у нужной команды.
//        Реализуйте эту логику.

//4. Создадим public класс CommandExecutor, через который можно будет взаимодействовать со всеми командами.
//        Создадим ему статическую карту Map<Operation, Command>, которую проинициализируем всеми известными нам
//        операциями и командами.
