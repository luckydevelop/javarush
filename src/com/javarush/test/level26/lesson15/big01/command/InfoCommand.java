package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.CurrencyManipulator;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulatorFactory;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by Lucky on 06.05.2015.
 */
class InfoCommand implements Command
{
    @Override
    public void execute()
    {
        Collection<CurrencyManipulator> manipulators = CurrencyManipulatorFactory.getAllCurrencyManipulators();

        boolean hasMoney=false;

        for (CurrencyManipulator man : manipulators)
        {
            if (man.hasMoney()) hasMoney = true;
        }

        if(!hasMoney) System.out.println("No money available.");

        else
        {
            for (CurrencyManipulator man : manipulators)
            {
                System.out.println(man.getCurrencyCode() + " - " + man.getTotalAmount());
            }

        }

    }
}

