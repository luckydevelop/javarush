package com.javarush.test.level26.lesson15.big01;

import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lucky on 03.04.2015.
 */
public class ConsoleHelper
{

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


    public static void writeMessage(String message)
    {
        System.out.println(message);
    }

    public static String readString() throws InterruptOperationException
    {
        String message = null;
        try
        {
            message = br.readLine();
            if(message.equals("exit")||message.equals("EXIT")) throw new InterruptOperationException();

        }
        catch (IOException ignor)
        {

        }

        return message;


    }

    public static String askCurrencyCode() throws IOException, InterruptOperationException
    {
        String test;
        writeMessage("choose.currency.code");
        while (true)
        {
            test = readString();
            if (test.length() == 3)
                break;
            else
                writeMessage("invalid.data");

        }
        test = test.toUpperCase();
        return test;
    }

    public static String[] getValidTwoDigits(String currencyCode) throws IOException, InterruptOperationException
    {
        String[] array;
        //writeMessage(String.format("choose.denomination.and.count.format"));
        writeMessage(String.format("choose.denomination.and.quantity"));

        while (true)
        {
            String s = readString();
            array = s.split(" ");
            int k;
            int l;
            try
            {
                k = Integer.parseInt(array[0]);
                l = Integer.parseInt(array[1]);
            }
            catch (Exception e)
            {
                writeMessage("invalid.data");
                continue;
            }
            if (k <= 0 || l <= 0 || array.length > 2)
            {
                writeMessage("invalid.data");
                continue;
            }
            break;
        }
        return array;
    }



    public static boolean checkWithRegExp(String Name)
    {
        Pattern p = Pattern.compile("^[1-4]$");
        Matcher m = p.matcher(Name);
        return m.matches();
    }

    public static Operation askOperation() throws  InterruptOperationException
    {
        String answer = null;
        while(true)
        {
            int operation;
            System.out.println("Введите номер операции 1- INFO, 2 - DEPOSIT, 3 - WITHDRAW, 4 - EXIT");
            answer = readString();

            if(checkWithRegExp(answer))
            {
                operation = Integer.parseInt(answer);
                return Operation.getAllowableOperationByOrdinal(operation);
            }
            else  System.out.println("ошибка, введите внимательней данные");

        }


        }

    public static void printExitMessage()
    {
        ConsoleHelper.writeMessage("the.end");
    }

}








//2. В классе ConsoleHelper реализуйте логику статического метода Operation askOperation()
//        Спросить у пользователя операцию.
//        Если пользователь вводит 1, то выбирается команда INFO, 2 - DEPOSIT, 3 - WITHDRAW, 4 - EXIT;
//        Используйте метод, описанный в п.1.
//        Обработай исключение - запроси данные об операции повторно.