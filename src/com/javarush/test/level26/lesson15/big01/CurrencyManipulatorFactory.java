package com.javarush.test.level26.lesson15.big01;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lucky on 02.05.2015.
 */
public final class CurrencyManipulatorFactory
{
    private static CurrencyManipulatorFactory ourInstance = new CurrencyManipulatorFactory();

    public static CurrencyManipulatorFactory getInstance()
    {
        return ourInstance;
    }

    private CurrencyManipulatorFactory()
    {
    }

    static Map<String, CurrencyManipulator> map = new HashMap<String, CurrencyManipulator>();

    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode)
    {
        if(map.containsKey(currencyCode)) return map.get(currencyCode);
        else
        {
            CurrencyManipulator newManip = new CurrencyManipulator(currencyCode);
            map.put(currencyCode, newManip);
            return newManip;
        }

    }


//   public static Map<String, CurrencyManipulator> getAllCurrencyManipulators()
//   {
//       return map;
//   }

    public static Collection<CurrencyManipulator> getAllCurrencyManipulators()
    {
        return map.values();
    }

}


//2.1. В классе CurrencyManipulatorFactory создайте статический метод getAllCurrencyManipulators(),
//        который вернет все манипуляторы.
//        У вас все манипуляторы хранятся в карте, не так ли? Если нет, то рефакторьте.
//        2.2. В InfoCommand в цикле выведите [код валюты - общая сумма денег для выбранной валюты]
//        Запустим прогу и пополним счет на EUR 100 2 и USD 20 6, и посмотрим на INFO.
//        Все работает правильно?
//        EUR - 200
//        USD - 120
//        Отлично!