package com.javarush.test.level25.lesson05.home01;

/**
 * Created by Lucky on 24.03.2015.
 */

// был пустой класс

public class LoggingStateThread extends Thread
{
    Thread target;
    public LoggingStateThread(Thread target)
    {
        this.target = target;
        setDaemon(true);

    }

    @Override
    public void run()
    {
        State state = this.target.getState();
        System.out.println(state);
        while (state != State.TERMINATED)
        {
            if (state != this.target.getState())
            {
                state = this.target.getState();
                System.out.println(state);
            }
        }
    }
}
//
//loggingStateThread.start();
//        target.start();  //NEW
//        Thread.sleep(100); //RUNNABLE
//        target.join(100);
//        Thread.sleep(400);
//        target.interrupted(); //TERMINATED
//        Thread.sleep(500);


//public class LoggingStateThread extends Thread
//{
//
//        public Thread tar2;
//        State state;
//
//        LoggingStateThread(Thread thread)
//        {
//            this.tar2 = thread;
//                setDaemon(true);
//        }
//
//        @Override
//        public void run()
//        {
//            state = tar2.getState();
//            System.out.println(state);
//
//             while(tar2.getState()!=state.TERMINATED)
//            {
//                if(state!=tar2.getState())
//                {
//                    state = tar2.getState();
//                    System.out.println(state);
//                }
//            }
//        }
//
//}


