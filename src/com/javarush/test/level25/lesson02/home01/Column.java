package com.javarush.test.level25.lesson02.home01;

import java.util.*;

public enum Column implements Columnable {
    Customer("Customer"),
    BankName("Bank Name"),
    AccountNumber("Account Number"),
    Amount("Available Amount");

    private String columnName;

    private static int[] realOrder;

    private Column(String columnName) {
        this.columnName = columnName;
    }

    /**
     * Задает новый порядок отображения колонок, который хранится в массиве realOrder.
     * realOrder[индекс в энуме] = порядок отображения; -1, если колонка не отображается.
     *
     * @param newOrder новая последовательность колонок, в которой они будут отображаться в таблице
     * @throws IllegalArgumentException при дубликате колонки
     */
    public static void configureColumns(Column... newOrder) {
        realOrder = new int[values().length];
        for (Column column : values()) {
            realOrder[column.ordinal()] = -1;
            boolean isFound = false;

            for (int i = 0; i < newOrder.length; i++) {
                if (column == newOrder[i]) {
                    if (isFound) {
                        //columnName - ???
                        throw new IllegalArgumentException("Column '" + column.columnName + "' is already configured.");
                    }
                    realOrder[column.ordinal()] = i;
                    isFound = true;
                }
            }
        }
    }

    /**
     * Вычисляет и возвращает список отображаемых колонок в сконфигурированом порядке (см. метод configureColumns)
     * Используется поле realOrder.
     *
     * @return список колонок
     */

        public static List<Column> getVisibleColumns() {
        List<Column> result = new LinkedList<>();
        Column[] val = values();

        int min = realOrder[0];
        int temp;
        Column tempVal;
        int[] realOrder2 = realOrder.clone(); // ???

        for (int i = 0; i < realOrder2.length; i++)
        {
           for (int j = i+1; j < realOrder2.length; j++)
            {
                if(realOrder2[i]>realOrder2[j])
                {
                    temp = realOrder2[i];
                    realOrder2[i] = realOrder2[j];
                    realOrder2[j] = temp;

                    tempVal = val[i];
                    val[i] = val[j];
                    val[j] = tempVal;
                }
            }
        }




        for (int i = 0; i < realOrder2.length; i++)
        {
            if (realOrder2[i]>-1) result.add(val[i]);

        }

        return result;
    }



//    public static List<Column> getVisibleColumns() {
//        List<Column> result = new ArrayList<>();
//        Map<Integer, Column> map = new TreeMap<>();
//        for (int i = 0; i < realOrder.length; i++) {
//            map.put(realOrder[i], Column.values()[i]);
//        }
//
//        for (Map.Entry<Integer, Column> entry : map.entrySet()){
//            if (entry.getKey() != -1)
//                result.add(entry.getValue());
//        }
//        return result;
//    }


    /**
     * @return полное имя колонки
     */
    @Override
    public String getColumnName()
    {
        return columnName;
    }

    /**
     * Возвращает true, если колонка видимая, иначе false
     */
    @Override
    public boolean isShown()
    {
        if (realOrder[this.ordinal()]==-1) return false;
        else return true;
    }


    /**
     * Скрывает колонку - маркирует колонку -1 в массиве realOrder.
     * Сдвигает индексы отображаемых колонок, которые идут после скрытой
     */
    @Override
    public void hide()
    {
      realOrder[this.ordinal()] = -1;
     }


}





//package com.javarush.test.level25.lesson02.home01;
//
//        import java.util.LinkedList;
//        import java.util.List;
//
//public enum Column implements Columnable {
//    Customer("Customer"),
//    BankName("Bank Name"),
//    AccountNumber("Account Number"),
//    Amount("Available Amount");
//
//    private String columnName;
//
//    private static int[] realOrder;
//
//    private Column(String columnName) {
//        this.columnName = columnName;
//    }
//
//    /**
//     * Задает новый порядок отображения колонок, который хранится в массиве realOrder.
//     * realOrder[индекс в энуме] = порядок отображения; -1, если колонка не отображается.
//     *
//     * @param newOrder новая последовательность колонок, в которой они будут отображаться в таблице
//     * @throws IllegalArgumentException при дубликате колонки
//     */
//    public static void configureColumns(Column... newOrder) {
//        realOrder = new int[values().length];
//        for (Column column : values()) {
//            realOrder[column.ordinal()] = -1;
//            boolean isFound = false;
//
//            for (int i = 0; i < newOrder.length; i++) {
//                if (column == newOrder[i]) {
//                    if (isFound) {
//                        throw new IllegalArgumentException("Column '" + column.columnName + "' is already configured.");
//                    }
//                    realOrder[column.ordinal()] = i;
//                    isFound = true;
//                }
//            }
//        }
//    }
//
//    /**
//     * Вычисляет и возвращает список отображаемых колонок в сконфигурированом порядке (см. метод configureColumns)
//     * Используется поле realOrder.
//     *
//     * @return список колонок
//     */
//    public static List<Column> getVisibleColumns() {
//        List<Column> result = new LinkedList<>();
//
//        return result;
//    }
//
//
//    @Override
//    public String getColumnName()
//    {
//        return null;
//    }
//
//    @Override
//    public boolean isShown()
//    {
//        return false;
//    }
//
//    @Override
//    public void hide()
//    {
//
//    }
//}
