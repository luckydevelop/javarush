package com.javarush.test.level21.lesson08.task02;

/* Клонирование
Класс Plant не должен реализовывать интерфейс Cloneable
Реализуйте механизм глубокого клонирования для Tree.
*/
public class Solution {
    public static void main(String[] args)
    {
        Tree tree = new Tree("willow", new String[]{"s1", "s2", "s3", "s4"});
        Tree clone = null;
        try {
            clone = tree.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println(tree);
        System.out.println(clone);

        System.out.println(tree.branches);
        System.out.println(clone.branches);

        for( String s : tree.branches ) System.out.println(s);
        for( String s : clone.branches ) System.out.println(s);
    }

    public static class Plant
    {
        private String name;

        public Plant(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class Tree extends Plant implements Cloneable
    {
        private String[] branches;

        public Tree(String name, String[] branches) {
            super(name);
            this.branches = branches;
        }

        public String[] getBranches() {
            return branches;
        }

        @Override
        protected Tree clone() throws CloneNotSupportedException
        {
             String name = this.getName();
            if(this.branches==null)
            {
               return  new Tree(name, null);
            }

            String[] branches = this.branches.clone();
                Tree clone2 = new Tree(name, branches);
                return clone2 ;


            //else return new Tree(name, null);

        }
    }
}



//public class Solution
//{
//    public static void main(String[] args)
//    {
//        Tree tree = new Tree("willow", new String[]{"s1", "s2", "s3", "s4"});
//        Tree clone = null;
//        try
//        {
//            clone = tree.clone();
//        }
//        catch (CloneNotSupportedException e)
//        {
//            e.printStackTrace();
//        }
//
//        System.out.println(tree);
//        System.out.println(clone);
//
//        System.out.println(tree.branches);
//        System.out.println(clone.branches);
//
//
//    }
//
//    public static class Plant
//    {
//        private String name;
//
//        public Plant(String name)
//        {
//            this.name = name;
//        }
//
//        public String getName()
//        {
//            return name;
//        }
//    }
//
//    public static class Tree extends Plant implements Cloneable
//    {
//        private String[] branches;
//
//        public Tree(String name, String[] branches)
//        {
//            super(name);
//            this.branches = branches;
//        }
//
//        public String[] getBranches()
//        {
//            return branches;
//        }
//
//    }
//}


