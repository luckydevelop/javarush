package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;

public class Hippodrome
{


    public static Hippodrome game;


    public static ArrayList<Horse> horses = new ArrayList<Horse>();

    public ArrayList<Horse> getHorses()
    {
        return horses;
    }

    public void move ()
    {
        for(int i=0; i<horses.size(); i++)
        {
            horses.get(i).move();
        }

    }


    public void print ()
    {
        for(int i=0; i<horses.size(); i++)
        {
            horses.get(i).print();
            System.out.println();
            System.out.println();

        }

    }



    public void run () throws InterruptedException
    {
        for(int i = 0; i<=10; i++ )
        {
            move();
            print();


                Thread.sleep(500);

        }
    }

    public Horse getWinner()
    {
        Horse winner = null;
        double maxDistance=0;
        for(Horse h : horses)
        {
            if(h.distance>maxDistance)
            {
                maxDistance = h.distance;
                winner = h;
            }

        }

        return winner;
    }

    public void printWinner()
    {
        System.out.println("Winner is " +getWinner().name+ "!");

    }

    public static void main(String[] args) throws InterruptedException
    {
        game = new Hippodrome();
        Horse h1 = new Horse("h1", 3, 0);
        Horse h2 = new Horse("h2", 3, 0);
        Horse h3 = new Horse("h3", 3, 0);
        game.horses.add(h1);
        game.horses.add(h2);
        game.horses.add(h3);

        game.run();
        game.printWinner();
    }

}

