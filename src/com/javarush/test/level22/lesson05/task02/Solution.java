package com.javarush.test.level22.lesson05.task02;

/* Между табуляциями
Метод getPartOfString должен возвращать подстроку между первой и второй табуляцией.
На некорректные данные бросить исключение TooShortStringException.
Класс TooShortStringException не менять.
*/
public class Solution {
    public static void main(String[] args) throws TooShortStringException
    {
        System.out.println(getPartOfString("A\tB\tC\tD\tE\tF\tG\tH\tI"));
        System.out.println(getPartOfString("A\tB"));
    }

    public static String getPartOfString(String string) throws TooShortStringException
    {
        if(string==null || string.isEmpty() || string.indexOf("\t")<0) throw new TooShortStringException();

        String s1="";

            int indexFirstTab = string.indexOf("\t");

        if( (string.indexOf("\t", indexFirstTab+1)<0)) throw new TooShortStringException();

            int indexSecondTab = string.indexOf("\t", indexFirstTab + 1);
            s1 = string.substring(indexFirstTab + 1, indexSecondTab);



        return s1;
    }

    public static class TooShortStringException extends Exception {
    }
}


//public class Solution {
//    public static String getPartOfString(String string) {
//        return null;
//    }
//
//    public static class TooShortStringException extends Exception {
//    }
//}