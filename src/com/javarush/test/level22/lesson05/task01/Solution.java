package com.javarush.test.level22.lesson05.task01;

/* Найти подстроку
Метод getPartOfString должен возвращать подстроку начиная с символа после 1-го пробела и до конца слова,
которое следует после 4-го пробела.
Пример: "JavaRush - лучший сервис обучения Java."
Результат: "- лучший сервис обучения"
На некорректные данные бросить исключение TooShortStringException (сделать исключением).
Сигнатуру метода getPartOfString не менять.
*/

public class Solution {
    public static void main(String[] args) throws TooShortStringException
    {
        System.out.println(getPartOfString("JavaRush - лучший сервис обучения"));
    }

    public static String getPartOfString(String string) throws TooShortStringException
    {
        String s2 = "";
        try
        {

            int firstSpace = string.indexOf(" ");
            String s1 = string.substring(firstSpace + 1);
            int fiveSpace = firstSpace;
            for(int i=0; i<4; i++)
            {
                fiveSpace = string.indexOf(" ", fiveSpace + 1);
            }

            s2 = string.substring(firstSpace + 1, fiveSpace);

        }

        catch(Exception e)
        {
            throw new TooShortStringException();
        }

        return s2;
    }

    public static class TooShortStringException extends Exception
    {

    }
}

//public class Solution {
//    public static String getPartOfString(String string) {
//        return null;
//    }
//
//    public static class TooShortStringException {
//    }
//}
