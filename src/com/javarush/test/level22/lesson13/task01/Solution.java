package com.javarush.test.level22.lesson13.task01;

import java.util.StringTokenizer;

/* StringTokenizer
Используя StringTokenizer разделить query на части по разделителю delimiter.
Пример,
getTokens("level22.lesson13.task01", ".") == {"level22", "lesson13", "task01"}
*/
public class Solution {
    public static void main(String[] args)
    {
        String[] string = getTokens("level22.lesson13.task01", ".");
        for(String s : string)
        {
            System.out.println(s);
        }
    }

    public static String [] getTokens(String query, String delimiter)
    {
        StringTokenizer st = new StringTokenizer(query, delimiter);
        int n = st.countTokens();
        String[] s2 =new String[n];



        //System.out.println(n);
        int i=0;
        while(st.hasMoreElements())
        {
            s2[i] = st.nextToken();
           // s2[i] = (String)st.nextElement();
            i++;
            //System.out.println(st.nextElement());

        }

        String s = st.toString();


        return s2;
    }
}


//public class Solution {
//    public static String [] getTokens(String query, String delimiter) {
//        return null;
//    }
//}
