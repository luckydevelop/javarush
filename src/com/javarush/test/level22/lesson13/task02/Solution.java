package com.javarush.test.level22.lesson13.task02;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/* Смена кодировки
В метод main первым параметром приходит имя файла, тело которого в кодировке Windows-1251.
В метод main вторым параметром приходит имя файла, в который необходимо записать содержимое первого файла в кодировке UTF-8.
*/
public class Solution  {
    static String win1251TestString = "РќР°СЂСѓС€РµРЅРёРµ РєРѕРґРёСЂРѕРІРєРё РєРѕРЅСЃРѕР»Рё?"; //only for your testing



    public static void main(String[] args) throws IOException
    {
        String fileIn = args[0];
        String fileOut = args[1];

//        String fileIn = "e://1.txt";
//        String fileOut = "e://2.txt";
        Charset utf = Charset.forName("UTF-8");
        Charset win = Charset.forName("Windows-1251");

        FileInputStream fis = new FileInputStream(fileIn);
        FileOutputStream fos = new FileOutputStream(fileOut);

        byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        String sWin = new String(buffer, utf);
        buffer = sWin.getBytes(win);
        fos.write(buffer);

        byte[] bufferTest = win1251TestString.getBytes(win);
        String sTest = new String(bufferTest, utf );
        System.out.println(sTest);

        fis.close();
        fos.close();


    }
}
