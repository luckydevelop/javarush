package com.javarush.test.level22.lesson13.task03;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Проверка номера телефона
Метод checkTelNumber должен проверять, является ли аргумент telNumber валидным номером телефона.
Критерии валидности:
1) если номер начинается с '+', то он содержит 12 цифр
2) если номер начинается с цифры или открывающей скобки, то он содержит 10 цифр
3) может содержать 0-2 знаков '-', которые не могут идти подряд
4) может содержать 1 пару скобок '(' и ')'  , причем если она есть, то она расположена левее знаков '-'
5) скобки внутри содержат четко 3 цифры
6) номер не содержит букв
7) номер заканчивается на цифру

Примеры:
+380501234567 - true
+38(050)1234567 - true
+38050123-45-67 - true
050123-4567 - true

+38)050(1234567 - false
+38(050)1-23-45-6-7 - false
050ххх4567 - false
050123456 - false
(0)501234567 - false
*/

//public class Solution {
//    public static void main(String[] args) throws IOException
//    {
//        Scanner sc = new Scanner(new File("c:\\file1.txt"));
//        List<String> list = new ArrayList<>();
//
//        while (sc.hasNextLine()) {
//            String s = sc.nextLine();
//            list.add(s.substring(0, s.indexOf(' ')));
//        }
//        sc.close();
//
//        for (String s : list) {
//            System.out.println(s + " " + checkTelNumber(s));
//        }
//    }
//
//    public static boolean checkTelNumber(String telNumber) {
//        if (telNumber == null) return false;
//        if (!telNumber.matches(".*\\d$")) return false;
//        if (telNumber.matches(".*[^\\d\\(\\)\\-\\+].*")) return false;
//        if (telNumber.matches(".*\\(.*"))  {
//            if (!telNumber.matches("^\\+?\\d*(?:\\(\\d{3}\\))+[\\d\\-]*\\d+$")) return false;
//        }
//
//        if (telNumber.matches(".*\\-.*"))  {
//            if (!telNumber.matches("^\\+?(?:\\d+)?(?:\\(\\d{3}\\))?(?:(\\+|[0-9]|(\\(\\d{3}\\))){1}\\-){1}\\d?(?:[0-9]+\\-)?\\d+$")) return false;
//        }
//
//        if (telNumber.matches("^\\+[\\d\\(\\)\\-].*") && telNumber.replaceAll("\\D","").matches("\\d{12}"))
//            return true;
//        if (telNumber.matches("^(\\d|\\(\\d{3}).*") && telNumber.replaceAll("\\D","").matches("\\d{10}"))
//            return true;
//
//        return false;
//    }
//
//}


public class Solution {

    public static void main(String[] args) throws FileNotFoundException
    {

        Scanner sc = new Scanner(new File("e://1.txt"));
        List<String> list = new ArrayList<>();

        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            list.add(s.substring(0, s.indexOf(" ")));
        }
        sc.close();

        for (String s : list) {
            System.out.println(s + " " + checkTelNumber(s));
        }
    }




    public static boolean checkTelNumber(String telNumber)
    {
        boolean res = false;
        boolean u6;

        if(telNumber=="" || telNumber==null) return false;

        //6) номер не содержит букв
        String s = telNumber.replaceAll("[^\\p{Alpha}]","");
        //if ((s.length()>0)&&)
            if ((s.length()==0)&&(telNumber.matches("^.*\\d$"))&&telNumber.matches("^(\\+\\d{2})?(\\(?\\d{3}\\)?\\d{3})((-\\d+){0,2})?(\\d{4})?"))
                return true;

        //7) номер заканчивается на цифру
//        if(telNumber.matches("^.*\\d$")) res = true;
//        else res = false;

        //1) если номер начинается с '+', то он содержит 12 цифр
       // if(telNumber.matches("^\\+\\d{12}$")) res = true;

        //2) если номер начинается с цифры или открывающей скобки, то он содержит 10 цифр
                //(050)1234567
                //0501234567
            //^\d?\(?\d{3}\)?\d{7}$
               // telNumber.matches("^\\d?\\(?\\d{3}\\)?\\d{7}$");

//        if(telNumber.matches("^\\d+")||telNumber.matches("^\\(\\d{3}\\).*"))
//        {
//            String s1 = telNumber.replaceAll("\\D","");
//            if(s1.length()==10)
//                res = true;
//        }

        //3) может содержать 0-2 знаков '-', которые не могут идти подряд
       // if(telNumber.matches("^(\\d+-){0,2}\\d+$")) return true;

        //4) может содержать 1 пару скобок '(' и ')'  , причем если она есть, то она расположена левее знаков '-'
       // if(telNumber.matches("^\\+?\\d+|\\(?\\d{3}\\)?(\\d+-){0,2}\\d+$")) res = true;


        //if(telNumber.matches("\\d.*")) res = true;




        return res;
    }
}


//        3) может содержать 0-2 знаков '-', которые не могут идти подряд
//        4) может содержать 1 пару скобок '(' и ')'  , причем если она есть, то она расположена левее знаков '-'
//        5) скобки внутри содержат четко 3 цифры
//        6) номер не содержит букв
//        7) номер заканчивается на цифру