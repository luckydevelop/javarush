package com.javarush.test.level22.lesson09.task03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.*;

/* Составить цепочку слов
В методе main считайте с консоли имя файла, который содержит слова, разделенные пробелом.
В методе getLine используя StringBuilder расставить все слова в таком порядке,
чтобы последняя буква данного слова совпадала с первой буквой следующего не учитывая регистр.
Каждое слово должно участвовать 1 раз.
Метод getLine должен возвращать любой вариант.
Слова разделять пробелом.
В файле не обязательно будет много слов.

Пример тела входного файла:
Киев Нью-Йорк Амстердам Вена Мельбурн

Результат:
Амстердам Мельбурн Нью-Йорк Киев Вена
*/
//public class Solution {
//    public static void main(String[] args) throws IOException
//    {
//        BufferedReader readerFileName = new BufferedReader(new InputStreamReader(System.in));
//        String fileName = readerFileName.readLine();
//        readerFileName.close();
//
//       // String fileName = "e://1.txt";
//        BufferedReader readerFile = new BufferedReader(new FileReader(fileName));
//        List<String> list2 = new ArrayList<String>();
//
//        while(readerFile.ready())
//        {
//            String s = readerFile.readLine();
//            String[] sArray = s.split(" ");
//            Collections.addAll(list2, sArray);
//            //for(String sTemp : sArray) list.add(sTemp);
//        }
//
//        readerFile.close();
//
//        String[] s5 = new String[list2.size()];
//        s5 = list2.toArray(s5);
//        //for(String l:s5) System.out.println(l);
//
//        //String s3 = list.toString();
//        //System.out.println(s3);
//
//
//        //...
//        StringBuilder result = getLine(s5);
//        System.out.println(result.toString());
//
//    }
//
//    public static StringBuilder getLine(String... words)
//    {
//
//        List<String> list = new ArrayList<String>();
//        Collections.addAll(list, words) ;
//
//
//
//        StringBuilder sb = new StringBuilder();
////        sb.append(list.get(0));
////        list.remove(0);
//
//
//String temp;
//
//        for (int i = 0; i < list.size(); i++)
//        {
//            char chI = list.get(i).charAt(list.get(i).length() - 1);
//            System.out.println(chI + "***");
//            for (int j = i+1; j < list.size(); j++)
//            {
//                //System.out.println(list.get(j));
//                String s6 = list.get(j).toLowerCase();
//                char chJ = s6.charAt(0);
//                System.out.println(chJ);
//                if (chI == chJ)
//                {
//                    temp = list.get(j);
//                    list.remove(j);
//                    list.add(i+1, temp);
//
//                }
//
//            }
//        }
//
//        for(String l:list)
//        {
//            sb.append(l+" ");
//        }
//
//
//        return sb;
//    }
//}
//

//public class Solution {
//    public static void main(String[] args) throws IOException
//    {
//        //...
//       // Scanner scanner = new Scanner(System.in);
//        //String fileName = scanner.nextLine();
//        //String fileName = "e://1.txt";
//        ArrayList<String> list = new ArrayList<>();
//        BufferedReader reader = new BufferedReader(new FileReader(fileName));
//        while (reader.ready())
//        {
//            String[] s = reader.readLine().split("\\s");
//            Collections.addAll(list, s);
//        }
//        reader.close();
//        String[] words = new String[list.size()];
//        words = list.toArray(words);
//        StringBuilder result = getLine(words);
//        System.out.println(result.toString());
//        //scanner.close();
//    }
//
//    public static StringBuilder getLine(String... words)
//    {
//        ArrayList<String> strings  = new ArrayList<>();
//        Collections.addAll(strings, words);
//        StringBuilder sb = new StringBuilder();
//        if (strings.size() == 0)
//            return new StringBuilder();
//
//        sb.append(strings.get(0));
//        strings.remove(0);
//
//        while (strings.size()>0){
//            for (int i = 0; i < strings.size(); i++) {
//                String a = strings.get(i).toUpperCase().toLowerCase();
//                String b = sb.toString().toUpperCase().toLowerCase();
//                if (a.charAt(0) == b.charAt(sb.length() - 1))
//                { // в конец
//                    sb.append(" ").append(strings.get(i));
//                    strings.remove(i);
//                    continue;
//                }
//
//                if (b.charAt(0) == a.charAt(a.length() - 1))
//                { //в начало
//                    sb.insert(0, " ");
//                    sb.insert(0, strings.get(i));
//                    strings.remove(i);
//                }
//            }
//        }
//        return sb;
//    }
//}


public class Solution
{
    public static void main(String[] args)
    {
        String[] wordsArray = new String[0];
        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String fileName = reader.readLine();
            reader.close();

            BufferedReader fileReader = new BufferedReader(new FileReader(fileName));

            List<String> words = new ArrayList<>();
            while (fileReader.ready())
            {
                String line = fileReader.readLine();
                words.addAll(Arrays.asList(line.split(" ")));
            }
            fileReader.close();
            wordsArray = words.toArray(new String[words.size()]);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        StringBuilder result = getLine(wordsArray);
        System.out.println(result.toString());
    }

    public static StringBuilder getLine(String... words)
    {
        StringBuilder resultStringBuilder = new StringBuilder();
        if (words == null)
            return resultStringBuilder;
        List<String> wordList = new ArrayList<>();
        Collections.addAll(wordList, words);

        int cycleCounter = 0;
        while (!wordList.isEmpty())
        {
            //находим первый случайный элемент
            if (resultStringBuilder.toString().equals(""))
            {
                int randomIndex = (int) (Math.random() * wordList.size());
                resultStringBuilder.append(wordList.get(randomIndex));
                wordList.remove(randomIndex);
            }

            //создаём временный лист для случайного перебора оставшихся элементов
            List<String> tempList = new ArrayList<>();
            tempList.addAll(wordList);
            //перебираем оставшиеся элементы в случайном порядке
            while (!tempList.isEmpty())
            {
                int randomIndex = (int) (Math.random() * tempList.size());
                String word = tempList.get(randomIndex);

                if (resultStringBuilder.toString().toLowerCase().charAt(resultStringBuilder.length() - 1) ==
                        word.toLowerCase().charAt(0))
                {
                    resultStringBuilder.append(" ").append(word);
                    //удаляем этот же элемент в основном листе
                    wordList.remove(word);

                }
                tempList.remove(randomIndex);
            }
            cycleCounter++;

            //если итераций уже прошло больше чем слов в изначальном списке, то повторяем всё заново
            if (cycleCounter > words.length)
            {
                wordList.clear();
                Collections.addAll(wordList, words);
                resultStringBuilder.delete(0, resultStringBuilder.length());
                cycleCounter = 0;
            }
        }

        return resultStringBuilder;
    }
}
