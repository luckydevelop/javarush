package com.javarush.test.level22.lesson09.task01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример, "мор"-"ром", "трос"-"сорт"
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br1.readLine();
       // String fileName = "e:/1.txt";

        List<String> list = new ArrayList<String>();

        BufferedReader bReader = new BufferedReader(new FileReader(fileName));

        while(bReader.ready())
        {
            String s = bReader.readLine();
            String[]sArray = s.split(" ");
            list.addAll(Arrays.asList(sArray));
           // System.out.println(s);
        }

        for(int i=0; i<list.size(); i++)
        {
            for(int j=i+1; j<list.size(); j++)
            {
                StringBuilder sb = new StringBuilder(list.get(j));
               //System.out.println(sb +" " +list.get(i));
                if (sb.reverse().toString().equals(list.get(i)))
                {
                   // System.out.println(list.get(j) +" " +list.get(i));
                    result.add(new Pair(list.get(j), list.get(i)));
                }
            }
        }

        for(Pair p : result) System.out.println(p);

    }

    public static class Pair {
        String first;
        String second;

        public Pair(String first, String second )
        {
            this.first = first;
            this.second = second;

        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
