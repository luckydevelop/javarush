package com.javarush.test.level17.lesson10.home09;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Транзакционность
Сделать метод joinData транзакционным, т.е. если произошел сбой, то данные не должны быть изменены.
1. Считать с консоли 2 имени файла
2. Считать построчно данные из файлов. Из первого файла - в allLines, из второго - в forRemoveLines
В методе joinData:
3. Если список allLines содержит все строки из forRemoveLines, то удалить из списка allLines все строки, которые есть в forRemoveLines
4. Если список allLines НЕ содержит каких-либо строк, которые есть в forRemoveLines, то
4.1. очистить allLines от данных
4.2. выбросить исключение CorruptedDataException
Сигнатуру метода main не менять.  Метод joinData должен вызываться в main.
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {
        //
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader readerFirst;
        BufferedReader readerSecond;

        try
        {
            //  читаю в буфер файлы
            readerFirst = new BufferedReader(new FileReader(reader.readLine()));
            readerSecond = new BufferedReader(new FileReader(reader.readLine()));
            String str1;
            String str2;
            // файл переганяю в колекцию
            while ((str1 = readerFirst.readLine()) != null)
            {
                allLines.add(str1);
            }
            while ((str2 = readerSecond.readLine()) != null)
            {
                forRemoveLines.add(str2);
            }
            System.out.println(allLines);
            System.out.println(forRemoveLines);
            try
            {
                // запускаю
                new Solution().joinData();
            }
            catch (CorruptedDataException e){}

        }
        catch (IOException ignore)
        {}

        System.out.println(allLines);
        System.out.println(forRemoveLines);
    }

    public  void joinData () throws CorruptedDataException{
        // если содержитВСЕ строки то удаляем ВСЕСтроки
        if (allLines.containsAll(forRemoveLines))
        {
            allLines.removeAll(forRemoveLines);
            return;
        }
        else {
            // иначе очиста и ошибка
            allLines.clear();
            throw new CorruptedDataException();
        }


    }

}


// Задание
//public class Solution {
//    public static List<String> allLines = new ArrayList<String>();
//    public static List<String> forRemoveLines = new ArrayList<String>();
//
//    public static void main(String[] args) throws IOException
//    {
//
//
//
//    }
//
//    public void joinData () throws CorruptedDataException {
//
//    }
//}




//Решение моё не прошло из-за статика в методе

//public class Solution {
//    public static List<String> allLines = new ArrayList<String>();
//    public static List<String> forRemoveLines = new ArrayList<String>();
//
//    public static void main(String[] args) throws IOException
//    {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName1 = reader.readLine();
//        String fileName2 = reader.readLine();
//        String s1, s2;
//
//
//        BufferedReader readerFile1 = new BufferedReader (new FileReader(fileName1));
//        {
//            while((s1=readerFile1.readLine())!=null)
//            {
//                allLines.add(s1);
//            }
//        }
//
//        BufferedReader readerFile2 = new BufferedReader (new FileReader(fileName2));
//        {
//
//
//            while((s2=readerFile2.readLine())!=null)
//            {
//                allLines.add(s2);
//            }
//        }
//
//        try
//        {
//            joinData();
//        }
//        catch (CorruptedDataException e)
//        {
//
//        }
//
//
//        for(String file1 : allLines)
//        {
//            System.out.println(file1);
//        }
//
//        for(String file1 : forRemoveLines)
//        {
//            System.out.println(file1);
//        }
//
//
//    }
//
//    public static void joinData () throws CorruptedDataException
//    {
//        if(allLines.containsAll(forRemoveLines)) allLines.removeAll(forRemoveLines);
//        else
//        {
//            allLines.clear();
//            throw new CorruptedDataException();
//        }
//    }