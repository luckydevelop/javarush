package com.javarush.test.level17.lesson10.bonus01;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* CRUD
CrUD - Create, Update, Delete
Программа запускается с одним из следующих наборов параметров:
-c name sex bd
-u id name sex bd
-d id
-i id
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с  - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
-u  - обновляет данные человека с данным id
-d  - производит логическое удаление человека с id
-i  - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)

id соответствует индексу в списке
Все люди должны храниться в allPeople
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat

Пример параметров: -c Миронов м 15/04/1990
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();
    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException
    {
        //start here - начни тут
        String param = args[0];
        String startWith = param.substring(1, 2);
        Person person;
        String name;
        String sex;
        Sex sexEnum;

        Date bd;
        SimpleDateFormat sdfParse = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

       switch (startWith)
       {
           case "c":
               name = args[1];
               if(args[2].equals("м")) sexEnum=Sex.MALE;
               else sexEnum=Sex.FEMALE;
               bd = sdfParse.parse(args[3]);
               if (sexEnum==Sex.MALE) person = Person.createMale(name, bd);
               else person =Person.createFemale(name, bd);
               allPeople.add(person);
               int index = allPeople.indexOf(person);
               System.out.println(index);
               break;


           case "u":
               name = args[2];
               if(args[3].equals("м")) sexEnum=Sex.MALE;
               else sexEnum=Sex.FEMALE;
               bd = sdfParse.parse(args[4]);
               person = allPeople.get(Integer.parseInt(args[1]));
               person.setName(name);
               person.setSex(sexEnum);
               person.setBirthDay(bd);
               allPeople.set(Integer.parseInt(args[1]), person);

               break;

           case "d":
               person = allPeople.get(Integer.parseInt(args[1]));
               person.setSex(null);
               person.setName(null);
               person.setBirthDay(null);

               break;

           case "i":
               person = allPeople.get(Integer.parseInt(args[1]));
               name = person.getName();
               if(person.getSex()==Sex.MALE) sex="м";
               else sex="ж";
               bd = person.getBirthDay();
               System.out.println(name+" "+sex+" "+sdf.format(bd));
               break;
       }




    }
}
