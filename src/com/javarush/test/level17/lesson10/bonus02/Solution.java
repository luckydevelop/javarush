package com.javarush.test.level17.lesson10.bonus02;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* CRUD 2
CrUD Batch - multiple Creation, Updates, Deletion
!!!РЕКОМЕНДУЕТСЯ выполнить level17.lesson10.bonus01 перед этой задачей!!!

Программа запускается с одним из следующих наборов параметров:
-c name1 sex1 bd1 name2 sex2 bd2 ...
-u id1 name1 sex1 bd1 id2 name2 sex2 bd2 ...
-d id1 id2 id3 id4 ...
-i id1 id2 id3 id4 ...
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с  - добавляет всех людей с заданными параметрами в конец allPeople, выводит id (index) на экран в соответствующем порядке
-u  - обновляет соответствующие данные людей с заданными id
-d  - производит логическое удаление всех людей с заданными id
-i  - выводит на экран информацию о всех людях с заданными id: name sex bd

id соответствует индексу в списке
Формат вывода даты рождения 15-Apr-1990
Все люди должны храниться в allPeople
Порядок вывода данных соответствует вводу данных
Обеспечить корректную работу с данными для множества нитей (чтоб не было затирания данных)
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();
    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException
    {
        //start here - начни тут
        String param = args[0].substring(1,2);
        String name;
        Sex sex;
        String sexString;
        Date bd;
        SimpleDateFormat sdfParse = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        Person person;
        int id;

        switch (param)
        {
            case "c":
                for (int i = 1; i < args.length; i=i+3)
                {
                    name = args[i];
                    bd = sdfParse.parse(args[i+2]);
                    if(args[i+1].equals("м"))
                    {
                        sex = Sex.MALE;
                        person = Person.createMale(name, bd);
                    }
                    else
                    {
                        sex = Sex.FEMALE;
                        person = Person.createFemale(name, bd);
                    }
                    allPeople.add(person);
                    id = allPeople.size()-1;
                    System.out.println(id);
                }

                break;




            case "u":
                for (int i = 1; i < args.length; i=i+4)
                {
                    id = Integer.parseInt(args[i]);
                    name = args[i+1];
                    if(args[i+2].equals("м"))
                    {
                        sex = Sex.MALE;
                    }
                    else
                    {
                        sex = Sex.FEMALE;
                    }
                    bd = sdfParse.parse(args[i+3]);

                    person = allPeople.get(id);
                    person.setName(name);
                    person.setSex(sex);
                    person.setBirthDay(bd);
                }

                break;


            case "d":
                for (int i = 1; i < args.length; i++)
                {
                    id = Integer.parseInt(args[i]);
                    person = allPeople.get(id);
                    person.setBirthDay(null);
                    person.setName(null);
                    person.setSex(null);
                }

                for (Person pers : allPeople)
                {
                    System.out.println(pers.getName()+" "+pers.getSex());
                }

                break;


            case "i":
                for (int i = 1; i < args.length; i++)
                {
                    id = Integer.parseInt(args[i]);
                    person = allPeople.get(id);
                    name = person.getName();
                    if(person.getSex()==Sex.MALE) sexString = "м";
                    else sexString = "ж";
                    bd = person.getBirthDay();
                    System.out.println(name+" "+sexString+" "+sdf.format(bd));

                }

        }
    }
}



//public class Solution {
//    public static List<Person> allPeople = new ArrayList<Person>();
//    static {
//        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
//        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
//    }
//
//    public static void main(String[] args) throws ParseException
//    {
//        //start here - начни тут
//
//    }
//}
