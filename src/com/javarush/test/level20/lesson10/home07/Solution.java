package com.javarush.test.level20.lesson10.home07;

import java.io.*;

/* Переопределение сериализации в потоке
Сериализация/десериализация Solution не работает.
Исправьте ошибки не меняя сигнатуры методов и класса.
Метод main не участвует в тестировании.
*/


    public class Solution implements Serializable, AutoCloseable
    {
        transient private FileOutputStream stream;
        private String fileName;

        public Solution(String fileName) throws FileNotFoundException
        {
            this.fileName = fileName;
            this.stream = new FileOutputStream(fileName);
        }

        public static void main(String[] args) throws Exception
        {
            Solution s1 = new Solution("e://1.txt");
        s1.writeObject("1");
        s1.close();

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("e://3.txt"));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("e://3.txt"));

        oos.writeObject(s1);
        oos.close();
        Solution s2 = (Solution)ois.readObject();
        ois.close();
        System.out.println(s2.fileName);

        s2.writeObject("2");
        s2.close();

        }

        public void writeObject(String string) throws IOException
        {
            stream.write(string.getBytes());
            stream.write("\n".getBytes());
            stream.flush();
        }

        private void writeObject(ObjectOutputStream out) throws IOException
        {
            out.defaultWriteObject();

            out.writeObject(fileName);
           // out.close();

        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException
        {
            in.defaultReadObject();
            fileName = (String) in.readObject();
            stream = new FileOutputStream(fileName, true);
            //in.close();


        }

        @Override
        public void close() throws Exception
        {
            System.out.println("Closing everything!");
            stream.close();
        }
    }
//public class Solution implements Serializable, AutoCloseable {
//    private FileOutputStream stream;
//
//    public Solution(String fileName) throws FileNotFoundException {
//        this.stream = new FileOutputStream(fileName);
//    }
//
//    public void writeObject(String string) throws IOException {
//        stream.write(string.getBytes());
//        stream.write("\n".getBytes());
//        stream.flush();
//    }
//
//    private void writeObject(ObjectOutputStream out) throws IOException {
//        out.defaultWriteObject();
//        out.close();
//    }
//
//    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
//        in.defaultReadObject();
//        in.close();
//    }
//
//    @Override
//    public void close() throws Exception {
//        System.out.println("Closing everything!");
//        stream.close();
//    }
//}