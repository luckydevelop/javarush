package com.javarush.test.level20.lesson10.home05;

import com.javarush.test.level14.lesson08.home03.Person;

import java.io.*;
import java.util.logging.Logger;

/* Сериализуйте Person
Сериализуйте класс Person.
*/

public class Solution {


        public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        String fileName = "e://3.txt";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        Person p1 = new Person("Dimas", "Viktorovich", "Ukraine", Sex.MALE);
        oos.writeObject(p1);
        Person p2 = (Person)ois.readObject();

        System.out.println(p1);
        System.out.println(p2);
    }

    public static class Person implements Serializable{
        String firstName;
        String lastName;
        transient String fullName;
        transient final String greetingString;
        String country;
        Sex sex;
        transient PrintStream outputStream;
        transient Logger logger;

        Person(String firstName, String lastName, String country, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.greetingString = "Hello, ";
            this.country = country;
            this.sex = sex;
            this.outputStream = System.out;
            this.logger = Logger.getLogger(String.valueOf(Person.class));
        }
        public void writeObject (ObjectOutputStream oos) throws IOException
        {
            oos.writeObject(firstName);
            oos.writeObject(lastName);
            oos.writeObject(country);
            oos.writeObject(sex);
        }
        public void readObject (ObjectInputStream ois) throws IOException, ClassNotFoundException
        {
            firstName = (String)ois.readObject();
            lastName = (String)ois.readObject();
            country = (String)ois.readObject();
            sex = (Sex)ois.readObject();
            fullName = String.format("%s, %s", lastName, firstName);
        }

        public String toString ()
        {
            return String.format("%s %s %s %s ", greetingString, fullName, country, sex  );
        }

    }

    enum Sex {
        MALE,
        FEMALE
    }
}


//public class Solution {
//
//    public static class Person  {
//        String firstName;
//        String lastName;
//        String fullName;
//        final String greetingString;
//        String country;
//        Sex sex;
//        PrintStream outputStream;
//        Logger logger;
//
//        Person(String firstName, String lastName, String country, Sex sex) {
//            this.firstName = firstName;
//            this.lastName = lastName;
//            this.fullName = String.format("%s, %s", lastName, firstName);
//            this.greetingString = "Hello, ";
//            this.country = country;
//            this.sex = sex;
//            this.outputStream = System.out;
//            this.logger = Logger.getLogger(String.valueOf(Person.class));
//        }
//    }
//
//    enum Sex {
//        MALE,
//        FEMALE
//    }
//}