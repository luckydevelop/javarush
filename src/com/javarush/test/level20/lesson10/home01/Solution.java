package com.javarush.test.level20.lesson10.home01;

import java.io.*;

/* Минимум изменений
Используя минимум изменений кода сделайте так, чтобы сериализация класса C стала возможной.
*/
public class Solution
{
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        String filename = "e://3.txt";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));

        C c1 = new C("C");
        oos.writeObject(c1);
        C c2 = (C)ois.readObject();


    }

    public static class A implements Serializable {
        String name = "A";



        public A(String name) {
            this.name += name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static class B extends A {
        String name = "B";



        public B(String name) {
            super(name);
            this.name += name;
        }
    }

    public static class C extends B   {
        String name = "C";



        public C(String name) {
            super(name);
            this.name = name;
        }
    }
}


//public class Solution
//{
//
//    public class A {
//        String name = "A";
//
//        public A(String name) {
//            this.name += name;
//        }
//
//        @Override
//        public String toString() {
//            return name;
//        }
//    }
//
//    public class B extends A {
//        String name = "B";
//
//        public B(String name) {
//            super(name);
//            this.name += name;
//        }
//    }
//
//    public class C extends B {
//        String name = "C";
//
//        public C(String name) {
//            super(name);
//            this.name = name;
//        }
//    }
//}
