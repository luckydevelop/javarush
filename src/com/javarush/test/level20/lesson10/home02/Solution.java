package com.javarush.test.level20.lesson10.home02;

import java.io.*;

/* Десериализация
На вход подается поток, в который записан сериализованный объект класса A либо класса B.
Десериализуйте объект в методе getOriginalObject предварительно определив, какого именно типа там объект.
Реализуйте интерфейс Serializable где необходимо.
*/
public class Solution implements Serializable {
    public A getOriginalObject(ObjectInputStream objectStream) throws IOException, ClassNotFoundException
    {
        A result = null;
        String s = objectStream.readObject().getClass().getSimpleName();

        if(s.equals("A")) result = new A();
        if(s.equals("B")) result = new B();

//        if(s.equals("A")) result = (A)objectStream.readObject();
//        if(s.equals("B")) result = (B)objectStream.readObject();

        return result;
    }

    public class A implements Serializable {
        public A ()
        {}
    }

    public class B extends A {
        public B()
        {
            System.out.println("inside B");
        }
    }
}


//public class Solution {
//    public A getOriginalObject(ObjectInputStream objectStream)
//    {
//
//        return a;
//    }
//
//    public class A implements {
//    }
//
//    public class B extends A {
//        public B() {
//            System.out.println("inside B");
//        }
//    }
//}