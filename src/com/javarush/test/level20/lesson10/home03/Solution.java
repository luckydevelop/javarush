package com.javarush.test.level20.lesson10.home03;

import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
*/

public class Solution implements Serializable  {

    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        String fileName = "e://3.txt";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        Solution s = new Solution();
        B b1 = s.new B("B");
        oos.writeObject(b1);
        oos.close();

        B b2 = (B)ois.readObject();
        ois.close();
        System.out.println(b1.name);
        System.out.println(b2.name);

//        Solution s = new Solution();
//        B b = s.new B("B");
//        System.out.println(b.name);
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("e://3.txt"));
//        objectOutputStream.writeObject(b);
//        objectOutputStream.close();
//        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("e://3.txt"));
//        B bDes;
//        bDes = (B) objectInputStream.readObject();
//        System.out.println("b:    " + b.toString() + " " + b.name);
//        System.out.println("bDes: " + bDes.toString() + " " + bDes.name);
//        objectInputStream.close();

    }

    public static class A {
        public A()
        {

        }
        protected String name = "A";

        public A(String name) {
            this.name += name;
        }
    }

    public  class B extends A implements Serializable {
        public B(String name) {
            super(name);
            this.name += name;
        }

        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException
        {
            ois.defaultReadObject();
            name = (String)ois.readObject();

        }

        private void writeObject(ObjectOutputStream oos) throws IOException
        {
            oos.defaultWriteObject();
            oos.writeObject(name);
        }
    }
}



//public class Solution {
//    public static class A {
//        protected String name = "A";
//
//        public A(String name) {
//            this.name += name;
//        }
//    }
//
//    public class B extends A implements Serializable {
//        public B(String name) {
//            super(name);
//            this.name += name;
//        }
//    }
//}
