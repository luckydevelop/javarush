package com.javarush.test.level20.lesson10.bonus03;

import java.util.ArrayList;
import java.util.List;

/* Кроссворд
1. Дан двумерный массив, который содержит буквы английского алфавита в нижнем регистре.
2. Метод detectAllWords должен найти все слова из words в массиве crossword.
3. Элемент(startX, startY) должен соответствовать первой букве слова, элемент(endX, endX) - последней.
text - это само слово, располагается между начальным и конечным элементами
4. Все слова есть в массиве.
5. Слова могут быть расположены горизонтально, вертикально и по диагонали как в нормальном, так и в обратном порядке.
6. Метод main не участвует в тестировании
*/



//public class Solution {
//    public static void main(String[] args) {
//        int[][] crossword = new int[][]{
//                {'f', 'd', 'e', 'r', 'l', 'k'},
//                {'u', 's', 'a', 'm', 'e', 'o'},
//                {'l', 'n', 'g', 'r', 'o', 'v'},
//                {'m', 'l', 'p', 'r', 'r', 'h'},
//                {'p', 'o', 'e', 'e', 'j', 'j'}
//        };
//        List<Word> list = detectAllWords(crossword, "pmluf", "kovhj", "jhvok", "lprr", "rrpl", "jv");
//        for (Word word : list) System.out.println(word);
//        /*
//Ожидаемый результат
//home - (5, 3) - (2, 0)
//same - (1, 1) - (4, 1)
//         */
//    }
//
//    public static List<Word> detectAllWords(int[][] crossword, String... words)
//    {
//        List<Word> word = new ArrayList<>();
//        for (String s : words)
//        {
//            char[] wordSS = s.toCharArray();
//            for (int i = 0; i < crossword.length; i++)
//                for (int j = 0; j < crossword[i].length; j++)
//                {
//                    if (wordSS[0] == crossword[i][j])
//                    {
//                        String str = "";
//                        Word keyword = new Word(str);
//                        int len = crossword.length-1; //4  / длина масива crossword
//                        int wordLen = wordSS.length-1; //1 / кол-во букв в слове
//                        // находим совпадение по вертикали вниз
//                        if ((len - i) >= wordLen && crossword[i + 1][j] == wordSS[1])   //проверка на длину символов и совпадение с последующим
////                        // символом
//                     //    if ((i<=crossword.length-1) && crossword[i + 1][j] == wordSS[1])   //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = i;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[k][j] == wordSS[l])
//                            {
//                                str += (char) crossword[k][j];
//                                keyword = new Word(str);
//                                 System.out.println("k = " + k + "  " + "j = " + j + "  ");
//                                k++;
//                                l++;
//                            }
//                            k--;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(j, k);
//                            if (str.equals(s))word.add(keyword);
//                            //System.out.println(keyword+"ver+");
//                        }
//                        //проверка по вертикали назад
//                        if (i >= wordLen && crossword[i - 1][j] == wordSS[1])   //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = i;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[k][j] == wordSS[l])
//                            {
//                                str += (char) crossword[k][j];
//                                keyword = new Word(str);
//                                //System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k--;
//                                l++;
//                            }
//                            k++;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(j, k);
//                            if (str.equals(s))word.add(keyword);
//                            // System.out.println(keyword+"ver-");
//                        }
//                        //проверка по горизонтали вперед
//                        if ((crossword[i].length-1 - j) >= wordLen && crossword[i][j+1] == wordSS[1])     //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = j;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[i][k] == wordSS[l])
//                            {
//                                str += (char) crossword[i][k];
//                                keyword = new Word(str);
//                                // System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k++;
//                                l++;
//                            }
//                            k--;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(k, i);
//                            if (str.equals(s))word.add(keyword);
//                            // System.out.println(keyword+"hor+");
//                        }
//                        //проверка по горизонтали назад
//                        if (j >= wordLen && crossword[i][j - 1] == wordSS[1])     //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = j;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[i][k] == wordSS[l])
//                            {
//                                str += (char) crossword[i][k];
//                                keyword = new Word(str);
//                                // System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k--;
//                                l++;
//                            }
//                            k++;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(k, i);
//                            if (str.equals(s))word.add(keyword);
//                            // System.out.println(keyword+"hor-");
//                        }
//
//                        //проверка по диагонали вправо-вниз
//                        if ((len - i) >= wordLen && (crossword[i].length-1 - j) >= wordLen && crossword[i+1][j+1] == wordSS[1])     //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = i;
//                            int m = j;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[k][m] == wordSS[l])
//                            {
//                                str += (char) crossword[k][m];
//                                keyword = new Word(str);
//                                // System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k++;
//                                m++;
//                                l++;
//                            }
//                            k--;
//                            m--;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(m, k);
//                            if (str.equals(s))word.add(keyword);
//                            // System.out.println(keyword+"diag_vpravo_vniz+");
//                        }
//                        //проверка по диагонали вверх влево
//                        if (i >= wordLen && j >= wordLen && crossword[i-1][j-1] == wordSS[1])     //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = i;
//                            int m = j;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[k][m] == wordSS[l])
//                            {
//                                str += (char) crossword[k][m];
//                                keyword = new Word(str);
//                                //System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k--;
//                                m--;
//                                l++;
//                            }
//                            k++;
//                            m++;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(m, k);
//                            if (str.equals(s)) word.add(keyword);
//                            // System.out.println(keyword+"diag_vverh_vlevo-");
//                        }
//                        //проверка по диагонали вверх вправо
//                        if (i >= wordLen && (crossword[i].length-1 - j) >= wordLen && crossword[i-1][j+1] == wordSS[1])     //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = j;
//                            int m = i;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[k][m] == wordSS[l])
//                            {
//                                str += (char) crossword[k][m];
//                                keyword = new Word(str);
//                                //System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k--;
//                                m++;
//                                l++;
//                            }
//                            k++;
//                            m--;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(m, k);
//                            if (str.equals(s)) word.add(keyword);
//                            // System.out.println(keyword+"diag_vverh_vpravo+");
//                        }
//                        //проверка по диагонали вверх вправо
//                        if ((len - i) >= wordLen && j >= wordLen && crossword[i+1][j-1] == wordSS[1])     //проверка на длину символов и совпадение с последующим
//                        // символом
//                        {
//                            int k = i;
//                            int m = j;
//                            int l = 0;
//                            str = "";
//                            while (l <= wordLen && crossword[k][m] == wordSS[l])
//                            {
//                                str += (char) crossword[k][m];
//                                keyword = new Word(str);
//                                //System.out.println("k = " + k + "\t" + "j = " + j + "\t");
//                                k++;
//                                m--;
//                                l++;
//                            }
//                            k--;
//                            m++;
//                            keyword.setStartPoint(j, i);
//                            keyword.setEndPoint(m, k);
//                            if (str.equals(s)) word.add(keyword);
//                            // System.out.println(keyword+"diag_vniz_vlevo+");
//                        }
//                    }                                        //wordSS
//                }                                           //for crossword
//        }                                                  //for words
//        return word;
//    }
//
//    public static class Word {
//        private String text;
//        private int startX;
//        private int startY;
//        private int endX;
//        private int endY;
//
//        public Word(String text) {
//            this.text = text;
//        }
//
//        public void setStartPoint(int i, int j) {
//            startX = i;
//            startY = j;
//        }
//
//        public void setEndPoint(int i, int j) {
//            endX = i;
//            endY = j;
//        }
//
//        @Override
//        public String toString() {
//            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
//        }
//    }
//}




public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'},
        };
                //List<Word> l = detectAllWords(crossword, "same", "emas", "kovhj", "jhvok", "rek", "rrj", "emo", "emoh", "rj", "rrj"  );
                //List<Word> l = detectAllWords(crossword, "home", "same", "emas", "kovhj", "rek");
        List<Word> l = detectAllWords(crossword,  "rwj"  );
        for(Word wo : l) System.out.println(wo);
        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {

        List<Word> list = new ArrayList<Word>();

        char chars[][] = new char[words.length][];

        for(int i=0; i<chars.length; i++)
        {
            chars[i] = words[i].toCharArray();
        }

//        for(int i=0; i<chars.length; i++)
//        {
//            for(int j=0; j<chars[i].length; j++)
//            {
//                System.out.println(chars[i][j]);
//            }
//
//        }

        for (int a=0; a<chars.length; a++) //перебираем элекменты вложенного массива сhar (там где хранятся слова из words)
        {

            for (int i = 0; i < crossword.length; i++) // перебираем элементы вложеного масива crossword
            {
                for (int j = 0; j < crossword[i].length; j++)
                {


                    if(chars[a][0] == crossword[i][j])  // Если нашлась перваю буква в масиве crossword
                    {
                        //System.out.println("First letter - "+(char) crossword[i][j]);


                        int lenthCrosswords = crossword.length;
                        int lenthWord = chars[a].length;
                        int l = i;
                        int m =0;
                        String text ="";

//        //Движемся вниз
//                      // if((lenthCrosswords-1-i)>=(lenthWord-1)&&chars[a][1]==crossword[i+1][j])
//                        // if((lenthCrosswords-(i+1))>=lenthWord&&chars[a][1]==crossword[i+1][j])
//
//                    //     if ((len - i) >= wordLen && crossword[i + 1][j] == wordSS[1])
//                        if(((lenthCrosswords-1)-(i))>=(lenthWord-1)&&chars[a][1]==crossword[i+1][j])
//
//                       {
//
//                           while(m<lenthWord&&crossword[l][j]==chars[a][m])
//                          // while(crossword[l][j]==chars[a][m]&&m<lenthWord)
//                           {
//                               text +=chars[a][m];
//                               l++;
//                               m++;
//                           }
//
//                           //System.out.println((char) crossword[i+1][j]); // проверяем совпадения по вертикали вниз
//                           if(text.equals(words[a]))
//                           {
//                               Word w = new Word(text);
//                               w.setStartPoint(j, i);
//                               w.setEndPoint(j, l-1);
//                               list.add(w);
//                           }
//                       }
//
//     //Движемся вверх
//                        if(i+1>=lenthWord&&chars[a][1]==crossword[i-1][j])
//                        {
//                            while(m<lenthWord&&crossword[l][j]==chars[a][m])
//                            {
//                                text +=chars[a][m];
//                                l--;
//                                m++;
//                            }
//                            if(text.equals(words[a]))
//                            {
//                                Word w = new Word(text);
//                                w.setStartPoint(j, i);
//                                w.setEndPoint(j, l+1);
//                                list.add(w);
//                            }
//                        }
//
//     //Движемся вправо
//                        if(j+1<=lenthWord&&chars[a][1]==crossword[i][j+1])
//                        {
//                            int o = j;
//
//                            while(m<lenthWord&&crossword[i][o]==chars[a][m])
//                            {
//                                text +=chars[a][m];
//                                o++;
//                                m++;
//                            }
//
//                            if(text.equals(words[a]))
//                            {
//                                Word w = new Word(text);
//                                w.setStartPoint(j, i);
//                                w.setEndPoint(--o, i);
//                                list.add(w);
//                            }
//                        }
//
//      //Движемся влево
//                        if(j+1>lenthWord&&chars[a][1]==crossword[i][j-1])
//                        {
//                            int o = j;
//                            while(m<lenthWord&&crossword[i][o]==chars[a][m])
//                            {
//                                text +=chars[a][m];
//                                o--;
//                                m++;
//                            }
//                            if(text.equals(words[a]))
//                            {
//                                Word w = new Word(text);
//                                w.setStartPoint(j, i);
//                                w.setEndPoint(o+1, i);
//                                list.add(w);
//                            }
//                        }
//
//       //Движемся вправо вверх
//                        if(i+1>=lenthWord&&j+lenthWord<=(crossword[i].length) &&chars[a][1]==crossword[i-1][j+1])
//                        {
//                            System.out.println("ttt");
//                            int o = j;
//                            while(m<lenthWord&&crossword[l][o]==chars[a][m])
//                            {
//                                text +=chars[a][m];
//                                o++;
//                                l--;
//                                m++;
//                            }
//                            if(text.equals(words[a]))
//                            {
//                                Word w = new Word(text);
//                                w.setStartPoint(j, i);
//                                w.setEndPoint(++l, --o);
//                                list.add(w);
//                            }
//                        }

       //Движемся вправо вниз
//                        System.out.println("crossword[i].length = "+crossword[i].length);
//                        System.out.println("crossword.length = "+crossword.length);

                        if((crossword[i].length-j)>=lenthWord&&i+lenthWord<=(crossword.length) &&chars[a][1]==crossword[i+1][j+1])
                        {

                            int o = j;
                            while(m<lenthWord&&crossword[l][o]==chars[a][m])
                            {
                                text +=chars[a][m];
                                o++;
                                l++;
                                m++;
                                System.out.println("rrj "+text);
                            }
                            if(text.equals(words[a]))
                            {
                                Word w = new Word(text);
                                w.setStartPoint(j, i);
                                w.setEndPoint(++l, --o);
                                list.add(w);
                            }
                        }



                    }


                }
            }
        }


        return list;
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}




//public class Solution {
//    public static void main(String[] args) {
//        int[][] crossword = new int[][]{
//                {'f', 'd', 'e', 'r', 'l', 'k'},
//                {'u', 's', 'a', 'm', 'e', 'o'},
//                {'l', 'n', 'g', 'r', 'o', 'v'},
//                {'m', 'l', 'p', 'r', 'r', 'h'},
//                {'p', 'o', 'e', 'e', 'j', 'j'}
//        };
//        detectAllWords(crossword, "home", "same");
//        /*
//Ожидаемый результат
//home - (5, 3) - (2, 0)
//same - (1, 1) - (4, 1)
//         */
//    }
//
//    public static List<Word> detectAllWords(int[][] crossword, String... words) {
//
//        return null;
//    }
//
//    public static class Word {
//        private String text;
//        private int startX;
//        private int startY;
//        private int endX;
//        private int endY;
//
//        public Word(String text) {
//            this.text = text;
//        }
//
//        public void setStartPoint(int i, int j) {
//            startX = i;
//            startY = j;
//        }
//
//        public void setEndPoint(int i, int j) {
//            endX = i;
//            endY = j;
//        }
//
//        @Override
//        public String toString() {
//            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
//        }
//    }
//}