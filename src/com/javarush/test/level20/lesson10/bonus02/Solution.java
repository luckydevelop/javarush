package com.javarush.test.level20.lesson10.bonus02;

/* ���������-��������������
1. ��� ��������� ������ N*N, ������� �������� ��������� ���������������.
2. ��������� �������������� �� ������������� � �� �������������.
3. ������ ������������� ���� �������� 1.
4. � �������:
4.1) a[i, j] = 1, ���� ������� (i, j) ����������� ������-���� ��������������
4.2) a[i, j] = 0, � ��������� ������
5. getRectangleCount ������ ���������� ���������� ���������������.
6. ����� main �� ��������� � ������������
*/


public class Solution {
    public static void main(String[] args) {
        byte[][] a = new byte[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 1}
        };
        int count = getRectangleCount(a);
        System.out.println("count = " + count + ". ������ ���� 2");
        a = new byte[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {0, 0, 0, 0},
                {1, 1, 0, 1}
        };
        count = getRectangleCount(a);
        System.out.println("Count = " + count + ". ������ ���� 3" + " result: " + String.valueOf( count == 3));

        a = new byte[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 0}
        };
        count = getRectangleCount(a);
        System.out.println("Count = " + count + ". ������ ���� 1" + " result: " + String.valueOf( count == 1));

        a = new byte[][]{
                {1, 1, 0, 0, 0},
                {1, 1, 0, 1, 1},
                {1, 1, 0, 0, 0},
                {1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1}

        };
        count = getRectangleCount(a);
        System.out.println("Count = " + count + ". ������ ���� 3" + " result: " + String.valueOf( count == 3));

        a = new byte[][]{
                {1, 1, 0,},
                {1, 0, 0,},
                {0, 1, 1,},

        };
        count = getRectangleCount(a);
        System.out.println("Count = " + count + ". ������ ���� 2" + " result: " + String.valueOf( count == 2));

        a = new byte[][]{
                {1, 0, 0,},
                {0, 0, 0,},
                {0, 0, 1,},

        };
        count = getRectangleCount(a);
        System.out.println("Count = " + count + ". ������ ���� 2" + " result: " + String.valueOf( count == 2));

        a = new byte[][]{
                {1, 1, 0,},
                {0, 0, 0,},
                {0, 1, 1,},

        };
        count = getRectangleCount(a);
        System.out.println("Count = " + count + ". ������ ���� 2" + " result: " + String.valueOf( count == 2));
    }

    public static int getRectangleCount(byte[][] a)
    {
        int count =0;

        for(int i=0; i<a.length; i++)
        {
           // int i = i2;
            for(int j=0; j<a.length; j++)
            {
                //int j = j2;
               if(a[i][j]==1)
               {
                   int x1 = j;
                   int y1 = i;

                   while(i<a.length)
                   {
                       if(a[i][j]==0) break;
                       i++;
                   }
                   i--;

                   while(j<a.length)
                   {
                       if(a[i][j]==0) break;
                       j++;
                   }
                   j--;

                   for(int l=y1; l<=i; l++)
                   {
                       for(int k=x1; k<=j; k++)
                       {
                           a[l][k]=0;
                       }
                   }

                    count++;

                   j = x1;
                   i = y1;

               }
               //i = i2;
                //i2 = i;
                //j2 = j;

            }

        }
        return count;
    }
}



//public class Solution {
//    public static void main(String[] args) {
//        byte[][] a = new byte[][]{
//                {1, 1, 0, 0},
//                {1, 1, 0, 0},
//                {1, 1, 0, 0},
//                {1, 1, 0, 1}
//        };
//        int count = getRectangleCount(a);
//        System.out.println("count = " + count + ". ������ ���� 2");
//        a = new byte[][]{
//                {1, 1, 0, 0},
//                {1, 1, 0, 0},
//                {0, 0, 0, 0},
//                {1, 1, 0, 1}
//        };
//        count = getRectangleCount(a);
//        System.out.println("Count = " + count + ". ������ ���� 3" + " result: " + String.valueOf( count == 3));
//
//        a = new byte[][]{
//                {0, 0, 0, 0},
//                {0, 0, 0, 0},
//                {1, 1, 0, 0},
//                {1, 1, 0, 0}
//        };
//        count = getRectangleCount(a);
//        System.out.println("Count = " + count + ". ������ ���� 1" + " result: " + String.valueOf( count == 1));
//
//        a = new byte[][]{
//                {1, 1, 0, 0, 0},
//                {1, 1, 0, 1, 1},
//                {1, 1, 0, 0, 0},
//                {1, 1, 0, 0, 1},
//                {0, 0, 0, 0, 1}
//
//        };
//        count = getRectangleCount(a);
//        System.out.println("Count = " + count + ". ������ ���� 3" + " result: " + String.valueOf( count == 3));
//
//        a = new byte[][]{
//                {1, 1, 0,},
//                {1, 0, 0,},
//                {0, 1, 1,},
//
//        };
//        count = getRectangleCount(a);
//        System.out.println("Count = " + count + ". ������ ���� 2" + " result: " + String.valueOf( count == 2));
//
//        a = new byte[][]{
//                {1, 0, 0,},
//                {0, 0, 0,},
//                {0, 0, 1,},
//
//        };
//        count = getRectangleCount(a);
//        System.out.println("Count = " + count + ". ������ ���� 2" + " result: " + String.valueOf( count == 2));
//
//        a = new byte[][]{
//                {1, 1, 0,},
//                {0, 0, 0,},
//                {0, 1, 1,},
//
//        };
//        count = getRectangleCount(a);
//        System.out.println("Count = " + count + ". ������ ���� 2" + " result: " + String.valueOf( count == 2));
//    }
//
//    public static int getRectangleCount(byte[][] a)
//    {
//        int count = 0;
//
//        for (int i = 0; i < a.length; i++)
//            for (int j = 0; j < a.length; j++)
//            {
//                if (a[i][j]==1)
//                {
//                    int x1 = i;
//                    int y1 = j;
//                    while (i < a.length)
//                    {
//                        if (a[i][j]==0) break;
//                        i++;
//                    }
//                    i--;
//                    while (j < a.length)
//                    {
//                        if (a[i][j]==0) break;
//                        j++;
//                    }
//                    j--;
//                    for (int l = x1; l <= i; l++)
//                        for (int k = y1; k <= j; k++)
//                            a[l][k]=0;
//                    count++;
//                }
//            }
//        return count;
//    }
//}


//public class Solution {
//    public static void main(String[] args) {
//        byte[][] a = new byte[][]{
//                {1, 1, 0, 0},
//                {1, 1, 0, 0},
//                {1, 1, 0, 0},
//                {1, 1, 0, 1}
//        };
//        int count = getRectangleCount(a);
//        System.out.println("count = " + count + ". ������ ���� 2");
//    }
//
//    public static int getRectangleCount(byte[][] a) {
//        return 0;
//    }
//}