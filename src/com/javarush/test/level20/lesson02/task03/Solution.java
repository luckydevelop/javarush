package com.javarush.test.level20.lesson02.task03;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* Знакомство с properties
В методе fillInPropertiesMap считайте имя файла с консоли и заполните карту properties данными из файла.
Про .properties почитать тут - http://ru.wikipedia.org/wiki/.properties
Реализуйте логику записи в файл и чтения из файла для карты properties.
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public static void main (String[] args) throws Exception
    {
        fillInPropertiesMap();

    }

    public static void fillInPropertiesMap() throws Exception
    {
        //implement this method - реализуйте этот метод
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       // String fileName = br.readLine();
        String fileName = "e://config.properties";

        InputStream inputStream = new FileInputStream(fileName);
        load(inputStream);
        for(Map.Entry<String, String>  pair : properties.entrySet()) System.out.println(pair.getKey()+ " "+pair.getValue());
        OutputStream outputStream= new FileOutputStream("e://test.properties");
        save(outputStream);

    }

    public static void save(OutputStream outputStream) throws Exception
    {
        //implement this method - реализуйте этот метод
        Properties prop = new Properties();

        for(Map.Entry<String, String> pair : properties.entrySet() )
        {
            prop.put(pair.getKey(), pair.getValue());
        }
        prop.store(outputStream, null);
        outputStream.close();
    }

    public static void load(InputStream inputStream) throws Exception
    {
        //implement this method - реализуйте этот метод
       // BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));
        Properties prop = new Properties();
        prop.load(inputStream);
        for(String key : prop.stringPropertyNames() )
        {
            properties.put(key, prop.getProperty(key));
        }

        inputStream.close();


    }
}
