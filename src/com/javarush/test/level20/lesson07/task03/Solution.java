package com.javarush.test.level20.lesson07.task03;

import java.io.*;
import java.util.List;

/* Externalizable Person
Класс Person должен сериализоваться с помощью интерфейса Externalizable.
Подумайте, какие поля не нужно сериализовать.
Исправьте ошибку сериализации.
Сигнатуры методов менять нельзя.
*/
public class Solution {

//    public static void main(String[] args) throws IOException, ClassNotFoundException
//    {
//        String fileName = "e://3.txt";
//        ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream(fileName));
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
//        Person p1 = new Person("Maks", "Ivanov", 33);
//        ous.writeObject(p1);
//        Person p2 = (Person)ois.readObject();
//
//    }

    public static class Person implements Externalizable {
        private String firstName;
        private String lastName;
        private int age;
        private Person mother;
        private Person father;
        private List<Person> children;
        //private static final long serialVersionUID = 1L;

        public Person()
        {

        }


        public Person(String firstName, String lastName, int age) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }

        public void setMother(Person mother) {
            this.mother = mother;
        }

        public void setFather(Person father) {
            this.father = father;
        }

        public void setChildren(List<Person> children) {
            this.children = children;
        }

        @Override
        public void writeExternal(ObjectOutput out) throws IOException {
            out.writeInt(age);
            out.writeObject(firstName);
            out.writeObject(lastName);
            out.writeObject(mother);
            out.writeObject(father);
            out.writeObject(children);
        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            age = in.readInt();
            firstName = (String)in.readObject();
            lastName = (String)in.readObject();


//            setMother((Person) in.readObject());
//            setFather((Person) in.readObject());
//            setChildren((List) in.readObject());
            mother = (Person)in.readObject();
            father = (Person)in.readObject();
            children = (List)in.readObject();
        }
    }
}

//public class Solution {
//    public static class Person implements Externalizable {
//        private String firstName;
//        private String lastName;
//        private int age;
//        private Person mother;
//        private Person father;
//        private List<Person> children;
//
//        public Person(String firstName, String lastName, int age) {
//            this.firstName = firstName;
//            this.lastName = lastName;
//            this.age = age;
//        }
//
//        public void setMother(Person mother) {
//            this.mother = mother;
//        }
//
//        public void setFather(Person father) {
//            this.father = father;
//        }
//
//        public void setChildren(List<Person> children) {
//            this.children = children;
//        }
//
//        @Override
//        public void writeExternal(ObjectOutput out) throws IOException {
//            out.writeObject(mother);
//            out.writeObject(father);
//            out.writeChars(firstName);
//            out.writeChars(lastName);
//            out.writeInt(age);
//            out.writeObject(children);
//        }
//
//        @Override
//        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//            firstName = in.readLine();
//            lastName = in.readLine();
//            father = (Person)in.readObject();
//            mother = (Person)in.readObject();
//            age = in.readInt();
//            children = (List)in.readObject();
//        }
//    }
//}