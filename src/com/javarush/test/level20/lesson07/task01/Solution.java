package com.javarush.test.level20.lesson07.task01;

import java.io.*;

/* Externalizable для апартаментов
Реализуйте интерфейс Externalizable для класса Apartment
Подумайте, какие поля не нужно сериализовать.
*/
public class Solution {

    public static void main (String[] args) throws IOException, ClassNotFoundException
    {
        String fileName = "e://1.txt";
        FileOutputStream fos = new FileOutputStream(fileName);
        FileInputStream fis = new FileInputStream(fileName);
        ObjectOutputStream out = new ObjectOutputStream(fos);
        ObjectInputStream ois = new ObjectInputStream(fis);

        Apartment app1 = new Apartment("Quebec", 911);
        app1.writeExternal(out);
        out.close();
        fos.close();

        Apartment app2 = new Apartment();
        app2.readExternal(ois);
        ois.close();
        fis.close();

        System.out.println(app1);
        System.out.println(app2);

    }

    public static class Apartment implements Externalizable {

        public Apartment() { super(); }

        public Apartment(String adr, int y) {
            address = adr;
            year = y;
        }

        private String address;
        private int year;

        @Override
        public void writeExternal(ObjectOutput out) throws IOException
        {
            out.writeObject(address);
            out.writeInt(year);
        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
        {
            address = (String)in.readObject();
            year = in.readInt();
        }



        /**
         * Mandatory public no-arg constructor.
         */


        /**
         * Prints out the fields. used for testing!
         */
        public String toString() {
            return("Address: " + address + "\n" + "Year: " + year);
        }
    }
}


//public class Solution {
//    public static void main(String[] args) throws IOException, ClassNotFoundException
//    {   FileOutputStream fos = new FileOutputStream("e://3.txt");
//        ObjectOutputStream oo = new ObjectOutputStream(fos);
//        Apartment ap1 = new Apartment("koneva 23", 1984);
//        ap1.writeExternal(oo);
//        System.out.println(ap1.toString());
//        oo.close(); fos.close();
//
//        FileInputStream fis = new FileInputStream("C:\\Users\\Russidze\\Desktop\\salva.txt");
//        ObjectInputStream ois = new ObjectInputStream(fis);
//        Apartment ap2 = new Apartment();
//        ap2.readExternal(ois);
//        System.out.println(ap2.toString());
//        ois.close();fis.close();
//    }
//
//    public static class Apartment implements Externalizable {
//
//        private String address;
//        private int year;
//
//        /**
//         * Mandatory public no-arg constructor.
//         */
//        public Apartment() { super(); }
//
//        public Apartment(String adr, int y) {
//            address = adr;
//            year = y;
//        }
//
//        /**
//         * Prints out the fields. used for testing!
//         */
//        public String toString() {
//            return("Address: " + address + "\n" + "Year: " + year);
//        }
//
//        @Override
//        public void writeExternal(ObjectOutput out) throws IOException
//        {
//            out.writeObject(address);
//            out.writeInt(year);
//        }
//
//        @Override
//        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
//        {
//            address = (String)in.readObject();
//            year = in.readInt();
//
//        }
//    }
//}