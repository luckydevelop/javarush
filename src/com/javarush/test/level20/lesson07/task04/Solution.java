package com.javarush.test.level20.lesson07.task04;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* Externalizable Solution
Сериализуйте класс Solution.
Подумайте, какие поля не нужно сериализовать.
Объект всегда должен содержать актуальные на сегодняшний день данные.
*/
public class Solution implements Serializable {
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        Solution s1 = new Solution(4);
        System.out.println(s1);
        String fileName = "e://3.txt";
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        oos.writeObject(s1);
        Solution s2 = (Solution)ois.readObject();
        System.out.println(s2);

//        String fileName = "e://3.txt";
//        ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream(fileName));
//        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
//        Solution solution1 = new Solution(8);
//        ous.writeObject(solution1);
//        Solution solution2 = (Solution)ois.readObject();
//        System.out.println(solution1);
//        System.out.println(solution2);

    }

    transient private final String pattern = "dd MMMM yyyy, EEEE";
    transient private Date currentDate;
    private int temperature;
    String string;

    public Solution() {};

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public String toString() {
        return this.string;
    }

}

//public class Solution {
//    public static void main(String[] args) {
//        System.out.println(new Solution(4));
//    }
//
//    private final String pattern = "dd MMMM yyyy, EEEE";
//    private Date currentDate;
//    private int temperature;
//    String string;
//
//    public Solution(int temperature) {
//        this.currentDate = new Date();
//        this.temperature = temperature;
//
//        string = "Today is %s, and current temperature is %s C";
//        SimpleDateFormat format = new SimpleDateFormat(pattern);
//        this.string = String.format(string, format.format(currentDate), temperature);
//    }
//
//    @Override
//    public String toString() {
//        return this.string;
//    }
//}
