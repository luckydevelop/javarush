package com.javarush.test.level09.lesson11.home04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/* Конвертер дат
Ввести с клавиатуры дату в формате «08/18/2013»
Вывести на экран эту дату в виде «AUG 18, 2013».
Воспользоваться объектом Date и SimpleDateFormat.
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        //Напишите тут ваш код

        Scanner sc = new Scanner(System.in);
        String dateS = sc.nextLine();

        Date date = new Date(dateS);

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

        String s = sdf.format(date).toUpperCase();

        System.out.println(s);













//        Date date = new Date("08/18/2013");
//        System.out.println(date);
//
//
//        SimpleDateFormat sf = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
//        //System.out.println(sf.format(date).toUpperCase());
//        System.out.println(sf.format(date).toUpperCase());

    }
}
