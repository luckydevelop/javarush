package com.javarush.test.level09.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Гласные и согласные буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа должна вывести на экран две строки:
1. первая строка содержит только гласные буквы
2. вторая - только согласные буквы и знаки препинания из введённой строки.
Буквы соединять пробелом.

Пример ввода:
Мама мыла раму.
Пример вывода:
а а ы а а у
М м м л р м .
*/

//public class Solution
//{
//    public static void main(String[] args) throws Exception
//    {
//        //Написать тут ваш код
//
//        InputStreamReader isr = new InputStreamReader(System.in);
//        BufferedReader br = new BufferedReader(isr);
//        String s = br.readLine();
//        char [] letters  = s.toCharArray();
//        String vowelStr = "";
//        String consonantStr = "";
//
//
//        for(char t : letters)
//        {
//            if (t == ' ')
//                continue;
//
//            if (isVowel(t))
//                vowelStr += t + " ";
//
//            else consonantStr += t + " ";
//
//        }
//
//            System.out.println(vowelStr.trim());
//            System.out.println(consonantStr.trim());
//
//
////            String ts = new String("t");
////
////            boolean testChar = isVowel(t);
////            if(testChar)
////            {
////                s1 =s1+ts+" ";
////            }
////            else if (ts.equals(" "))
////                s2 =s2+t+" ";
//
//
//
//
//
//    }
//
//
//    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};
//
//    //метод проверяет, гласная ли буква
//    public static boolean isVowel(char c)
//    {
//        c = Character.toLowerCase(c);  //приводим символ в нижний регистр - от заглавных к строчным буквам
//
//        for (char d : vowels)   //ищем среди массива гласных
//        {
//            if (c == d)
//                return true;
//        }
//        return false;
//    }
//}
