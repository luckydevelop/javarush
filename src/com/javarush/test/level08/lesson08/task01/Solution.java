package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        //Напишите тут ваш код

        HashSet<String> set = new HashSet<String>();

        set.add("Лук");
        set.add("Лиук");
        set.add("Лтук");
        set.add("Лутк");
        set.add("Лсук");
        set.add("Лувк");
        set.add("Лурвак");
        set.add("Лсмтук");
        set.add("Лсмук");
        set.add("Лаук");
        set.add("Лчук");
        set.add("Лтчсук");
        set.add("Лварук");
        set.add("Лавпррук");
        set.add("Лупрк");
        set.add("Лужлдк");
        set.add("Лаыук");
        set.add("Луваок");
        set.add("Лурапк");
        set.add("Лчтук");

        return set;

    }
}
