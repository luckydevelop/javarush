package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static void main(String[] args)
    {
        HashMap<String, String> map = createMap();
        int CountTheSameFirstName = getCountTheSameFirstName(map, "Олег");
        int CountTheSameLastName = getCountTheSameLastName(map, "Гончаренко");
        System.out.println(CountTheSameFirstName + " " + CountTheSameLastName);

    }

    public static HashMap<String, String> createMap()
    {
        //Напишите тут ваш код

        HashMap<String, String> maps = new HashMap<String, String>();
        maps.put("Гончаренко", "Олег");
        maps.put("Гончаренко", "Олег2");
        maps.put("Гончаренко", "Олег");
        maps.put("Гончаренко", "Олег");
        maps.put("Гончаренко", "Олег3");
        maps.put("Гончаренко5", "Олег");
        maps.put("Гончаренко6", "Олег");
        maps.put("Гончаренко7", "Олег");
        maps.put("Гончаренко8", "Олег");
        maps.put("Гончаренко9", "Олег");

        return maps;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        //Напишите тут ваш код
        int count = 0;

        for (Map.Entry<String, String> pair : map.entrySet())
        {
            String value = pair.getValue();
            if (name.equals(value))
            {
                count++;
            }
        }
        return count;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        //Напишите тут ваш код
        int count = 0;

        for (Map.Entry<String, String> pair : map.entrySet())
        {
            if (familiya.equals(pair.getKey()))
            {
                count++;
            }
        }
        return count;

    }
}


//    public static void main(String[] args)
//    {
//        HashMap<String, String> map = createMap();
//
//        System.out.println(getCountTheSameFirstName(map, "Vasilij"));
//        System.out.println(getCountTheSameLastName(map, "Ivanov"));
//    }
//
//    public static HashMap<String, String> createMap()
//    {
//        HashMap<String, String> people = new HashMap<String, String>();
//
//        people.put("Ivanov", "Petr");
//        people.put("Petrov", "Vasilij");
//        people.put("Ivanov2", "Vasilij");
//        people.put("Smirnov", "Pyotr");
//        people.put("Ivanov3", "Vasilij");
//        people.put("Ivanova", "Sofia");
//        people.put("Ivanovr", "John");
//        people.put("Smirnoff", "Vasilij");
//        people.put("Ivanov", "John");
//        people.put("Ivanov", "Buonaparte");
//
//        return people;
//    }
//
//    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
//    {
//        int count = 0;
//
//        for (Map.Entry<String, String> pair : map.entrySet())
//        {
//            String value = pair.getValue();
//            if (name.equals(value))
//            {
//                count++;
//            }
//        }
//
//        return count;
//    }
//
//    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
//    {
//        return map.containsKey(familiya) ? 1 : 0;
//    }
//
//
//}
