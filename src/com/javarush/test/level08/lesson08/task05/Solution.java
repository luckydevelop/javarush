package com.javarush.test.level08.lesson08.task05;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        //Напишите тут ваш код

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Kotov", "Vova1");
        map.put("Kotov1", "Vova2");
        map.put("Kotov2", "Vova3");
        map.put("Kotov3", "Vova4");
        map.put("Kotov4", "Vova");
        map.put("Kotov5", "Vova5");
        map.put("Kotov6", "Vova");
        map.put("Kotov7", "Vova");
        map.put("Kotov8", "Vova");
        map.put("Kotov9", "Vova");

        return map;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        //Напишите тут ваш код
        HashSet<String> names = new HashSet<String>();
        HashSet<String> namesDuplicate = new HashSet<String>();

        for (Map.Entry<String, String> pair : map.entrySet())
        {
            if(names.contains(pair.getValue()))
            {
                namesDuplicate.add(pair.getValue());
            }
            else
            {
                names.add(pair.getValue());
            }
        }

        for(String name : namesDuplicate)
        {
            removeItemFromMapByValue(map, name);
        }

        for (Map.Entry<String, String> pair : map.entrySet())
        {
            System.out.println(pair.getKey()+ "-" + pair.getValue());
        }





    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
