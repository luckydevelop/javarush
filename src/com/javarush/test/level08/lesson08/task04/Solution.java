package com.javarush.test.level08.lesson08.task04;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{


    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Сталлоне", new Date("JUNE 1 1980"));
        map.put("Сталлоне1", new Date("JUNE 2 1980"));
        map.put("Сталлоне2", new Date("JUNE 3 1980"));
        map.put("Сталлоне3", new Date("JUNE 4 1980"));
        map.put("Сталлоне4", new Date("JUNE 5 1980"));
        map.put("Сталлоне5", new Date("OCTOBER 1 1980"));
        map.put("Сталлоне6", new Date("OCTOBER 2 1980"));
        map.put("Сталлоне7", new Date("OCTOBER 3 1980"));
        map.put("Сталлоне8", new Date("OCTOBER 4 1980"));
        map.put("Сталлоне9", new Date("OCTOBER 5 1980"));

        //Напишите тут ваш код

//        for( Map.Entry<String, Date> pair : map.entrySet() )
//        {
//            System.out.println(pair.getKey() +" " + pair.getValue());
//        }

        return map;

    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        //Напишите тут ваш код

        Iterator<Map.Entry<String, Date>> iterator = map.entrySet().iterator();

       while (iterator.hasNext())
       {
           Date month = iterator.next().getValue();
           int n = month.getMonth()+1;
           if(n>=6&&n<=8)
               iterator.remove();
       }

    }


    public static void main (String[] arg)
    {
        HashMap<String, Date> map = createMap();

        removeAllSummerPeople(map);

        for( Map.Entry<String, Date> pair : map.entrySet() )
        {
            System.out.println(pair.getKey() +" " + pair.getValue());
        }

    }

}
