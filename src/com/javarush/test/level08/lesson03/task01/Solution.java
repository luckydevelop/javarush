package com.javarush.test.level08.lesson03.task01;

/* HashSet из растений
Создать коллекцию HashSet с типом элементов String.
Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
Вывести содержимое коллекции на экран, каждый элемент с новой строки.
Посмотреть, как изменился порядок добавленных элементов.
*/

import java.util.HashSet;
import java.util.Set;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        Set<String> sets = new HashSet<String>();
        sets.add("арбуз");
        sets.add("банан");
        sets.add("вишня");
        sets.add("груша");
        sets.add("дыня");
        sets.add("ежевика");
        sets.add("жень-шень");
        sets.add("земляника");
        sets.add("ирис");
        sets.add("картофель");

        for(String set : sets) System.out.println(set);


    }
}
