package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static final boolean MALE = true;
    public static final boolean FEMALE = true;

    public static void main(String[] args)
    {
        //Написать тут ваш код

        Human[] family = new Human[9];
        ArrayList<Human> children = new ArrayList<Human>();

        family[0] = new Human("Dite1", MALE, 5);
        family[1] = new Human("Dite2", FEMALE, 6);
        family[2] = new Human("Dite3", MALE, 7);

        children.add(family[0]);
        children.add(family[1]);
        children.add(family[2]);

        family[3] = new Human("Mama", FEMALE, 30, children);
        family[4] = new Human("Papa", MALE, 34, children);

        children = new ArrayList<Human>();
        children.add(family[3]);

        family[5] = new Human("Baba1", FEMALE, 60, children);
        family[6] = new Human("Deda1", MALE, 60, children);

        children = new ArrayList<Human>();
        children.add(family[4]);

        family[7] = new Human("Baba2", FEMALE, 63, children);
        family[8] = new Human("Deda2", MALE, 63, children);

        for(Human person : family) System.out.println(person);
    }

    public static class Human
    {
        //Написать тут ваш код
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;


        public Human(String name, boolean sex, int age)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = new ArrayList<Human>();
        }

        public Human(String name, boolean sex, int age, ArrayList<Human> children)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
