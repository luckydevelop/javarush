package com.javarush.test.level08.lesson11.home03;

import java.util.HashMap;
import java.util.Map;

/* Люди с одинаковыми именами и/или фамилиями
1. Создать словарь Map (<String, String>) и добавить туда 10 человек в виде «Фамилия»-«Имя».
2. Пусть среди этих 10 человек есть люди с одинаковыми именами.
3. Пусть среди этих 10 человек есть люди с одинаковыми фамилиями.
4. Вывести содержимое Map на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Map<String, String> map = createPeopleList();
        printPeopleList(map);
    }

    public static Map<String, String> createPeopleList()
    {
        //Напишите тут ваш код
        Map<String, String> peopleList = new HashMap<String, String>();
        peopleList.put("Oleg", "Goncharenko");
        peopleList.put("Oleg", "Goncharenko");
        peopleList.put("Oleg", "Goncharenko1");
        peopleList.put("Oleg", "Goncharenko2");
        peopleList.put("Oleg", "Goncharenko3");
        peopleList.put("Oleg", "Goncharenko4");
        peopleList.put("Oleg7", "Goncharenko5");
        peopleList.put("Oleg", "Goncharenko6");
        peopleList.put("Oleg", "Goncharenko7");
        peopleList.put("Oleg", "Goncharenko8");

        return peopleList;
    }

    public static void printPeopleList(Map<String, String> map)
    {
        for (Map.Entry<String, String> s : map.entrySet())
        {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }

}
