package com.javarush.test.level08.lesson11.home09;

import java.util.Date;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года -
нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args)
    {
        boolean odd = isDateOdd("JUNE 29 2014");
        System.out.println(odd);
       // Date test = new Date();
        //long teset = userDate.getDay;
        //System.out.println(test.getDay);
    }

    public static boolean isDateOdd(String date)
    {
        Date newYearDate = new Date();
        newYearDate.setMonth(0);
        newYearDate.setDate(1);

        Date userDate = new Date(date);

        long msInDay = 24*60*60*1000;

        long difference = userDate.getTime()-newYearDate.getTime();

        int daysFromNewYear = (int)(difference/msInDay);



         System.out.println(daysFromNewYear);

        return  daysFromNewYear%2==0 ? false : true;
    }
}
