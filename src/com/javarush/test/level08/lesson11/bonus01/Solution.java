package com.javarush.test.level08.lesson11.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here - напиши код тут
        Scanner sc = new Scanner(System.in);
        String month = sc.nextLine();

        ArrayList<String> months = new ArrayList<String>();
        months.add( "January" );
        months.add( "February" );
        months.add( "March" );
        months.add( "April" );
        months.add( "May" );
        months.add( "June" );
        months.add( "July" );
        months.add( "August" );
        months.add( "September" );
        months.add( "October" );
        months.add( "November" );
        months.add( "December" );

        for ( String n : months )
        {
            if (month.contains(n))
            {
                System.out.println(month +" is " + (months.indexOf(n)+1) + " month");
            }
        }


    }

}
