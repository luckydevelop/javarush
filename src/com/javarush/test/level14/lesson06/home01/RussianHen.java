package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Lucky on 28.12.2014.
 */
public class RussianHen extends Hen implements Country
{
    @Override
    int getCountOfEggsPerMonth()
    {
        return 5;
    }

    @Override
    public String getDescription()
    {
        return super.getDescription() + String.format(" Моя страна - %s. Я несу %d яиц в месяц.", Country.RUSSIA, getCountOfEggsPerMonth() ) ;
    }

}
