package com.javarush.test.level14.lesson08.home05;

/**
 * Created by Lucky on 06.01.2015.
 */
public class Keyboard implements CompItem
{
    @Override
    public String getName()
    {
        return this.getClass().getSimpleName();
    }
}
