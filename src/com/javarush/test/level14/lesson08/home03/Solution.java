package com.javarush.test.level14.lesson08.home03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* User, Looser, Coder and Proger
1. Ввести [в цикле] с клавиатуры несколько строк (ключей).
Строки(ключи) могут быть такими: "user", "looser", "coder", "proger".
Ввод окончен, когда строка не совпадает ни с одной из выше указанных.

2. Для каждой введенной строки нужно:
2.1. Создать соответствующий объект [см Person.java], например, для строки "user" нужно создать объект класса User.
2.2. Передать этот объект в метод doWork.

3. Написать реализацию метода doWork, который:
3.1. Вызывает метод live() у переданного обекта, если этот объект (person) имеет тип User.
3.2. Вызывает метод doNothing(), если person имеет тип Looser.
3.3. Вызывает метод coding(), если person имеет тип Coder.
3.4. Вызывает метод enjoy(), если person имеет тип Proger.
*/

public class Solution implements Person
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        Person person = null;
        String key = null;

        //тут цикл по чтению ключей, пункт 1
        List<String> lists = new ArrayList<String>();

        while(true)
        {
            key = reader.readLine();
            if(key.equals("user")||key.equals("looser")||key.equals("coder")||key.equals("proger")) lists.add(key);
            else break;

        }

        {
        //создаем объект, пункт 2
            List<Person> persons = new ArrayList<Person>();

            for(String list : lists)
            {
                if(list.equals("user")) doWork(new User());
                if(list.equals("looser")) doWork(new Looser());
                if(list.equals("coder")) doWork(new Coder());
                if(list.equals("proger")) doWork(new Proger());
            }


        doWork(person); //вызываем doWork

        }
    }

    public static void doWork(Person person)
    {
        // пункт 3

        if (person instanceof User) ((User) person).live();
        if (person instanceof Looser) ((Looser) person).doNothing();
        if (person instanceof Coder) ((Coder) person).coding();
        if (person instanceof Proger) ((Proger) person).enjoy();
    }
}
