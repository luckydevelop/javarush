package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Lucky on 04.01.2015.
 */
public interface Bridge
{
    int getCarsCount();
}

