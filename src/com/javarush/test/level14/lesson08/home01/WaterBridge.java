package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Lucky on 04.01.2015.
 */
public class WaterBridge implements Bridge
{
    @Override
    public int getCarsCount()
    {
        return 7;
    }
}
