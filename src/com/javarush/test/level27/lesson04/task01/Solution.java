package com.javarush.test.level27.lesson04.task01;

/* Создаем дедлок
Расставьте модификаторы так, чтобы при работе с этим кодом появился дедлок
Метод main порождает deadLock, поэтому не участвует в тестировании
*/
public class Solution {
    private final String field;

    public Solution(String field) {
        this.field = field;
    }

    public    String getField() {
        return field;
    }

    public  void sout(Solution solution) {
        System.out.format("111:  %s: %s %n", this.field, solution.getField());
        solution.sout2(this);
    }

    public  void sout2(Solution solution) {
        System.out.format("222:  %s: %s %n", this.field, solution.getField());
        solution.sout(this);
    }

    public static void main(String[] args) {
        final Solution solution = new Solution("first");
        final Solution solution2 = new Solution("second");
        new Thread(
                new Runnable()
                {
            public void run() {
                solution.sout(solution2);
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
                solution2.sout(solution);
            }
        }).start();
    }
}



//public class Deadlock {
//    static class Friend {
//        private final String name;
//        public Friend(String name) {
//            this.name = name;
//        }
//        public String getName() {
//            return this.name;
//        }
//        public synchronized void bow(Friend bower) {
//            System.out.format("%s: %s"
//                            + "  has bowed to me!%n",
//                    this.name, bower.getName());
//            bower.bowBack(this);
//        }
//        public synchronized void bowBack(Friend bower) {
//            System.out.format("%s: %s"
//                            + " has bowed back to me!%n",
//                    this.name, bower.getName());
//        }
//    }
//
//    public static void main(String[] args) {
//        final Friend alphonse =
//                new Friend("Alphonse");
//        final Friend gaston =
//                new Friend("Gaston");
//        new Thread(new Runnable() {
//            public void run() { alphonse.bow(gaston); }
//        }).start();
//        new Thread(new Runnable() {
//            public void run() { gaston.bow(alphonse); }
//        }).start();
//    }
//}


//public class Solution {
//    private final String field;
//
//    public Solution(String field) {
//        this.field = field;
//    }
//
//    public String getField() {
//        return field;
//    }
//
//    public void sout(Solution solution) {
//        System.out.format("111:  %s: %s %n", this.field, solution.getField());
//        solution.sout2(this);
//    }
//
//    public void sout2(Solution solution) {
//        System.out.format("222:  %s: %s %n", this.field, solution.getField());
//        solution.sout(this);
//    }
//
//    public static void main(String[] args) {
//        final Solution solution = new Solution("first");
//        final Solution solution2 = new Solution("second");
//        new Thread(new Runnable() {
//            public void run() {
//                solution.sout(solution2);
//            }
//        }).start();
//
//        new Thread(new Runnable() {
//            public void run() {
//                solution2.sout(solution);
//            }
//        }).start();
//    }
//}
