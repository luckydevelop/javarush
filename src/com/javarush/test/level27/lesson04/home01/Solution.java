package com.javarush.test.level27.lesson04.home01;

/* Модификаторы и дедлоки
Расставьте модификаторы так, чтобы при работе с этим кодом появился дедлок
*/

//HZ

public class Solution {
    public synchronized Object getData()
    {
        return new ThreadDeadlock().getData();
    }
}

//public class Solution {
//    public  Object getData()
//    {
//        return new ThreadDeadlock().getData();
//    }
//}
