package com.javarush.test.level24.lesson02.home01;

/**
 * Created by Lucky on 10.03.2015.
 */
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker
{

    public SelfInterfaceMarkerImpl()
    {

    }

    public void printS()
    {
        System.out.println("s");
    }

    public void printS2()
    {
        System.out.println("ss");
    }
}
