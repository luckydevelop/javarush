//package com.javarush.test.level13.lesson11.home03;
//
//import java.io.*;
//
///**
// * JavaRush.ru
// * Level 13, Lesson 11, Home 03
// * <p/>
// * 1. Считать с консоли имя файла.<b/>
// * 2. Вывести в консоль(на экран) содержимое файла.<b/>
// * 3. Не забыть закрыть файл и поток.
// * <p/>
// * Date: 29.04.13
//
// */
//



//public class Solution
//{
//    public static void main( String[] args ) throws IOException
//    {
//       // BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
//        String s = "C:/1.txt";
//       // InputStream inStream = new FileInputStream( br.readLine() );
//
//        InputStream inStream = new FileInputStream(s);
//        while ( inStream.available() > 0 )
//        {
//            System.out.print( ( char ) inStream.read() );
//        }
//
//        inStream.close();
//    }
//}


package com.javarush.test.level13.lesson11.home03;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть закрыть файл и поток.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader bfr = new BufferedReader(isr);

        InputStream is = new FileInputStream(bfr.readLine()) ;

        while (is.available()>0)
        {
            System.out.print( (char) is.read() );
        }

        is.close();




    }
}





//package com.javarush.test.level13.lesson11.home03;
//
///* Чтение файла
//1. Считать с консоли имя файла.
//2. Вывести в консоль(на экран) содержимое файла.
//3. Не забыть закрыть файл и поток.
//*/
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Solution
//{
//    public static void main(String[] args) throws IOException
//    {
//        //add your code here
//

//
//    }
//}
