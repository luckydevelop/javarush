package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

//public class Solution
//{
//    public static void main(String[] args) throws IOException
//    {
//        // напишите тут ваш код
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName = reader.readLine();
//
//        reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
//
//       List<Integer> lists = new ArrayList<Integer>();
//
//        while(reader.ready())
//        {
//            lists.add(Integer.parseInt(reader.readLine()));
//
////            String data = (String) inputStream.read();
////
////            if(data == ' ') continue;
////            else lists.add(data);
//
//            //System.out.println((char) inputStream.read());
//        }
//
//     //   for( Integer list : lists ) System.out.println(list);
//
//        for( int i=0; i<lists.size(); )
//        {
//            if(lists.get(i)%2!=0)
//            {
//                lists.remove(i);
//            }
//            else i++;
//        }
//
//       // for( Integer list : lists ) System.out.println(list);
//
//        for (int i = 0; i< lists.size(); i++)
//        {
//            int temp;
//
//            for (int j = 0; j < lists.size(); j++)
//            {
//                if(lists.get(i)<lists.get(j))
//                {
//                    temp = lists.get(i);
//                    lists.set(i, lists.get(j));
//                    lists.set(j, temp);
//
//                }
//
//            }
//        }
//
//        for( Integer list : lists ) System.out.println(list);
//
//
//
//
//    }
//}



public class Solution
{
    public static void main( String[] args ) throws IOException
    {
        BufferedReader reader = new BufferedReader( new InputStreamReader( System.in ) );
        String fileName = "1.txt";
        reader = new BufferedReader( new InputStreamReader( new FileInputStream( fileName ) ) );

        List<Integer> numbers = new ArrayList<Integer>();

        while ( reader.ready() )
        {
            int nextNumber = Integer.parseInt( reader.readLine() );
            if ( nextNumber % 2 == 0 )
            {
                numbers.add( nextNumber );
            }
        }

        reader.close();
        sort( numbers );

        for ( int x : numbers )
        {
            System.out.println( x );
        }
    }

    public static void sort( List<Integer> numbers )
    {
        for ( int i = 0; i < numbers.size() - 1; i++ )
        {
            for ( int j = 0; j < numbers.size() - i - 1; j++ )
            {
                if ( numbers.get( j ) > numbers.get( j + 1 ) )
                {
                    int tmp = numbers.get( j );
                    numbers.set( j, numbers.get( j + 1 ) );
                    numbers.set( j + 1, tmp );
                }
            }
        }
    }
}