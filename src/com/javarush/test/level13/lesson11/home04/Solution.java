
package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести все строки в файл, каждую строчку с новой стороки.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        OutputStream ops = new FileOutputStream(fileName);


        while(true)
        {
            String s = br.readLine();

            if(s.equals("exit"))
            {
                ops.write(s.getBytes());
                break;
            }

            ops.write((s+"\r").getBytes());
        }

        ops.close();
        br.close();



    }
}
