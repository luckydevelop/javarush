package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

public class Solution
{
    public Solution()
    {
    }

    public Solution(int a)
    {
    }

    public Solution(String a)
    {
    }

    Solution(double a)
    {
    }

    Solution(int a, int b)
    {
    }

    Solution(String a, String b)
    {
    }

    private Solution(char a)
    {
    }

    private Solution(Integer a, double b )
    {
    }

    private Solution(String a, int b)
    {
    }

    protected Solution(char a, int b)
    {
    }

    protected Solution(int a, char b)
    {
    }

    protected Solution(String a, double b)
    {
    }

}



//public class Solution
//{
//
//
//}
