package com.javarush.test.level15.lesson12.home05;

/**
 * Created by Lucky on 10.01.2015.
 */
public class SubSolution extends Solution
{
    public SubSolution()
    {
    }

    public SubSolution(int a)
    {
        super(a);
    }

    public SubSolution(String a)
    {
        super(a);
    }

     SubSolution(double a)
    {
        super(a);
    }

     SubSolution(int a, int b)
    {
        super(a, b);
    }

     SubSolution(String a, String b)
    {
        super(a, b);
    }

    private SubSolution(char a)
    {
        super(a);
    }

    private SubSolution(Integer a, Integer b )
    {
        super(a, b);
    }

    private SubSolution(String a, int b)
    {
        super(a, b);
    }

    protected SubSolution(char a, int b)
    {
        super(a, b);
    }

    protected SubSolution(int a, char b)
    {
        super(a, b);
    }

    protected SubSolution(String a, double b)
    {
        super(a, b);
    }
}
