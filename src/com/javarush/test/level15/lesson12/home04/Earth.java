package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Lucky on 10.01.2015.
 */
public class Earth implements Planet
{
    private static Earth ourInstance = null;

    private Earth()
    {
    }

    public static Earth getInstance()
    {
        if(ourInstance==null) ourInstance = new Earth();
        return ourInstance;
    }


}
