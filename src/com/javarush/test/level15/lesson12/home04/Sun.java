package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Lucky on 10.01.2015.
 */
public class Sun implements Planet
{
    private static Sun ourInstance = null;

    public static Sun getInstance()
    {
        if(ourInstance==null) ourInstance = new Sun();
        return ourInstance;
    }

    private Sun()
    {
    }
}
