package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Lucky on 10.01.2015.
 */
public class Moon implements Planet
{
    private static Moon ourInstance = null;

    public static Moon getInstance()
    {
        if(ourInstance==null) ourInstance = new Moon();
        return ourInstance;
    }

    private Moon()
    {
    }
}
