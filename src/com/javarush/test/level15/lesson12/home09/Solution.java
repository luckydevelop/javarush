package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String url = br.readLine();
       // String url = "http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo&obj=3.14&name=Amigo";

        String [] arrayUrls = url.split("\\?");
        arrayUrls = arrayUrls[1].split("&");

        for(String arrayUrl : arrayUrls)
        System.out.print(arrayUrl.split("=")[0] + " ");

        System.out.println();

        for(String arrayUrl : arrayUrls)
        {
            String[] obj = arrayUrl.split("=");
            if(obj[0].equals("obj"))
            {
                try
                {
                    alert (Double.parseDouble(obj[1]));
                }

                catch (Exception e)
                {
                    alert (obj[1]);
                }


            }

        }






    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}


//public class Solution
//{
//    public static void main( String[] args ) throws Exception
//    {
//        BufferedReader reader = new BufferedReader( new InputStreamReader( System.in ) );
//       // String[] parameters = reader.readLine().split( "\\?" );
//       //String st =  "http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo";
//       //String st = "http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo";
//        String st = "http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo&obj=3.14&name=Amigo";
//
//       // String[] parameters = reader.readLine().split( "\\?" );
//          String[] parameters = st.split( "\\?" );
//
//
//
//        if ( parameters.length < 2 )
//        {
//            return;
//        }
//
//        parameters = parameters[1].split( "&" );
//
//        for(String parameter : parameters) System.out.println(parameter);
//
//        for ( String s : parameters )
//        {
//            System.out.print( s.split( "=" )[0] + " " );
//        }
//        System.out.println();
//
//        for ( String s : parameters )
//        {
//            String[] nextParameter = s.split( "=" );
//
//            if ( "obj".equals( nextParameter[0] ) )
//            {
//                try
//                {
//                    alert( Double.parseDouble( nextParameter[1] ) );
//                }
//                catch ( NumberFormatException e )
//                {
//                    alert( nextParameter[1] );
//                }
//            }
//        }
//    }
//
//    public static void alert( double value )
//    {
//        System.out.println( "double " + value );
//    }
//
//    public static void alert( String value )
//    {
//        System.out.println( "String " + value );
//    }
//}



//public class Solution {
//    public static void main(String[] args) throws IOException
//    {
//        //add your code here