package com.javarush.test.level23.lesson04.task01;

//ХЗ не прошло!!! вставил другое решением. Отличия не могу найти

/* Inner
Реализовать метод getTwoSolutions, который должен возвращать массив из 2-х экземпляров класса Solution.
Для каждого экземпляра класса Solution инициализировать поле innerClasses двумя значениями.
Инициализация всех данных должна происходить только в методе getTwoSolutions.
*/
public class Solution {
    public InnerClass[] innerClasses = new InnerClass[2];

    public class InnerClass
    {
    }

    public static Solution[] getTwoSolutions()
    {

        Solution solution1 = new Solution();
        Solution solution2 = new Solution();
        solution1.innerClasses[0] = solution1.new InnerClass();
        solution1.innerClasses[1] = solution1.new InnerClass();
        solution2.innerClasses[0] = solution2.new InnerClass();
        solution2.innerClasses[1] = solution2.new InnerClass();
        return new Solution[]{solution1, solution2};


//        Solution[] solArray = new Solution[1];
//
//        Solution s1 = new Solution();
//        Solution s2 = new Solution();
//
//        Solution.InnerClass s1IC1 =  s1.new InnerClass();
//        Solution.InnerClass s1IC2 =  s1.new InnerClass();
//        s1.innerClasses[0] = s1IC1;
//        s1.innerClasses[1] = s1IC2;
//
//        Solution.InnerClass s2IC1 =  s2.new InnerClass();
//        Solution.InnerClass s2IC2 =  s2.new InnerClass();
//        s2.innerClasses[0] = s2IC1;
//        s2.innerClasses[1] = s2IC2;
//
//        solArray[0] = s1;
//        solArray[1] = s2;
//
//        return solArray;
    }
}


///* Inner
//Реализовать метод getTwoSolutions, который должен возвращать массив из 2-х экземпляров класса Solution.
//Для каждого экземпляра класса Solution инициализировать поле innerClasses двумя значениями.
//Инициализация всех данных должна происходить только в методе getTwoSolutions.
//*/
//public class Solution {
//    public InnerClass[] innerClasses = new InnerClass[2];
//
//    public class InnerClass {
//    }
//
//    public static Solution[] getTwoSolutions() {
//
//        return null;
//    }
//}
