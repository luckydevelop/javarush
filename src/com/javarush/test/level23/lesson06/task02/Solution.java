package com.javarush.test.level23.lesson06.task02;

/* Рефакторинг
Отрефакторите класс Solution: вынесите все константы в public вложенный(nested) класс Constants.
Запретите наследоваться от Constants.
*/
public class Solution {

    public final static class Constants
    {
        public final static String SERVER = "Server is not accessible for now.";
        public final static String USERAUTHO = "User is not authorized.";
        public final static String USERBAN = "User is banned.";
        public final static String ACCESS = "Access is denied.";
    }

    public class ServerNotAccessibleException extends Exception {
        public ServerNotAccessibleException() {
            super(Constants.SERVER);
        }

        public ServerNotAccessibleException(Throwable cause) {
            super(Constants.SERVER, cause);
        }
    }

    public class UnauthorizedUserException extends Exception {
        public UnauthorizedUserException() {
            super(Constants.USERAUTHO);
        }

        public UnauthorizedUserException(Throwable cause) {
            super(Constants.USERAUTHO, cause);
        }
    }

    public class BannedUserException extends Exception {
        public BannedUserException() {
            super(Constants.USERBAN);
        }

        public BannedUserException(Throwable cause) {
            super(Constants.USERBAN, cause);
        }
    }

    public class RestrictionException extends Exception {
        public RestrictionException() {
            super(Constants.ACCESS);
        }

        public RestrictionException(Throwable cause) {
            super(Constants.ACCESS, cause);
        }
    }
}


//public class Solution {
//
//    public final class Constants
//    {
//
//    }
//
//    public class ServerNotAccessibleException extends Exception {
//        public ServerNotAccessibleException() {
//            super("Server is not accessible for now.");
//        }
//
//        public ServerNotAccessibleException(Throwable cause) {
//            super("Server is not accessible for now.", cause);
//        }
//    }
//
//    public class UnauthorizedUserException extends Exception {
//        public UnauthorizedUserException() {
//            super("User is not authorized.");
//        }
//
//        public UnauthorizedUserException(Throwable cause) {
//            super("User is not authorized.", cause);
//        }
//    }
//
//    public class BannedUserException extends Exception {
//        public BannedUserException() {
//            super("User is banned.");
//        }
//
//        public BannedUserException(Throwable cause) {
//            super("User is banned.", cause);
//        }
//    }
//
//    public class RestrictionException extends Exception {
//        public RestrictionException() {
//            super("Access is denied.");
//        }
//
//        public RestrictionException(Throwable cause) {
//            super("Access is denied.", cause);
//        }
//    }
//}
