package com.javarush.test.level07.lesson09.task01;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Три массива
1. Введи с клавиатуры 20 чисел, сохрани их в список и рассортируй по трём другим спискам:
Число делится на 3 (x%3==0), делится на 2 (x%2==0) и все остальные.
Числа, которые делятся на 3 и на 2 одновременно, например 6, попадают в оба списка.
2. Метод printList должен выводить на экран все элементы списка  с новой строки.
3. Используя метод printList выведи эти три списка на экран. Сначала тот, который для x%3, потом тот, который для x%2,
потом последний.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //add your code here
        ArrayList<Integer> d3 = new ArrayList<Integer>();
        ArrayList<Integer> d2 = new ArrayList<Integer>();
        ArrayList<Integer> d = new ArrayList<Integer>();
        ArrayList<Integer> nlist = new ArrayList<Integer>();

        Scanner sc = new Scanner(System.in);

        for(int i=0; i<20; i++) nlist.add(sc.nextInt());

        for(int n : nlist)
        {
            if(n%3==0)
            {
                d3.add(n);
            }
            if(n%2==0)
            {
                d2.add(n);
            }
            if(n%3!=0&&n%2!=0)
            {
                d.add(n);
            }
        }

        printList (d3);
        printList (d2);
        printList (d);

    }

    public static void printList(List<Integer> lists) {
        //add your code here

        for(int list : lists) System.out.println(list);

    }
}
