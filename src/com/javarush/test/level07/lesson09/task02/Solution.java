package com.javarush.test.level07.lesson09.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/* 5 слов в обратном порядке
Введи с клавиатуры 5 слов в список строк. Выведи их в обратном порядке.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код

        Scanner sc = new Scanner(System.in);
        ArrayList<String> s = new ArrayList<String>();

        for(int i=0; i<5; i++) s.add(sc.nextLine());

//        for(int i = 0; i<=0; i--)
//        {
//            System.out.println(s.get(i));
//        }

        for(int i = s.size()-1; i>=0; i--)
        {
            System.out.println(s.get(i));
        }

    }
}
