package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и
 заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{

    static final boolean MALE = true;
    static final boolean FEMALE = false;

    public static void main(String[] args)
    {
        //Написать тут ваш код

        Human[] family = new Human[9];
        family [0] = new Human ("дед Вася", MALE, 50);
        family [1] = new Human ("дед Коля", MALE, 50);
        family [2] = new Human ("баба Маша", FEMALE, 50);
        family [3] = new Human ("баба Дуся", FEMALE, 50);
        family [4] = new Human ("папа", MALE, 50, family [0], family [3] );
        family [5] = new Human ("мама", FEMALE, 50, family [1], family [4]);
        family [6] = new Human ("олег", MALE, 50, family [4], family [5]);
        family [7] = new Human ("зоя", FEMALE, 50, family [4], family [5]);
        family [8] = new Human ("дима", MALE, 50, family [4], family [5]);

        for(int i=0; i<9; i++)
        {System.out.println(family[i]);}


    }

    public static class Human
    {
        //Написать тут ваш код
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        Human (String name, boolean sex, int age)
        {
            this(name, sex, age, null, null);
        }

        Human (String name, boolean sex, int age, Human father, Human mother)
        {
            this.name = name;
            this.sex=sex;
            this.age=age;
            this.father = father;
            this.mother = mother;
        }




        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
