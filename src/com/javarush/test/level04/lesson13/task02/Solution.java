package com.javarush.test.level04.lesson13.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
Пример: m=2, n=4
8888
8888
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        String m = sc.nextLine();
        String n = sc.nextLine();

        int m1 = Integer.parseInt(m);
        int n1 = Integer.parseInt(n);

        int v =8;

        for (int i = m1; i>0; i--)
        {
            for (int j = n1; j>0; j--)
            {
                System.out.print(v);
            }
            System.out.println();
        }


    }
}
