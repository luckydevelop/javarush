package com.javarush.test.level04.lesson10.task03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* Хорошего не бывает много
Ввести с клавиатуры строку и число N.
Вывести на экран строку N раз используя цикл while.
Пример ввода:
абв
2
Пример вывода:
абв
абв
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        String n = sc.nextLine();

        int n2 = Integer.parseInt(n);

        while (n2 > 0)
        {
            System.out.println(line);
            n2 = n2-1;

         }


    }
}
