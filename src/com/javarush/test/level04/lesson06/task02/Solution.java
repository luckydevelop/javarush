package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        String n1 = sc.nextLine();
        String n2 = sc.nextLine();
        String n3 = sc.nextLine();
        String n4 = sc.nextLine();

        int n1_1 = Integer.parseInt(n1);
        int n2_1 = Integer.parseInt(n2);
        int n3_1 = Integer.parseInt(n3);
        int n4_1 = Integer.parseInt(n4);

        if(n1_1>n2_1&&n1_1>n3_1&&n1_1>n4_1) System.out.print (n1_1);
          else if(n2_1>n1_1&&n2_1>n3_1&&n2_1>n4_1) System.out.print (n2_1);
             else if(n3_1>n1_1&&n3_1>n2_1&&n3_1>n4_1) System.out.print (n3_1);
        else System.out.print (n4_1);


    }
}
