package com.javarush.test.level04.lesson16.home04;

/* Меня зовут 'Вася'...
Ввести с клавиатуры строку name.
Ввести с клавиатуры дату рождения (три числа): y, m, d.
Вывести на экран текст:
«Меня зовут name
Я родился d.m.y»
Пример:
Меня зовут Вася
Я родился 15.2.1988
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        String name = br.readLine();

        String y = br.readLine();
        String m = br.readLine();
        String d = br.readLine();

        int y2 = Integer.parseInt(y);
        int m2 = Integer.parseInt(m);
        int d2 = Integer.parseInt(d);

        System.out.println("Меня зовут " + name + " Я родился " + d2 +"." + m2 + "." + y2);

    }
}
