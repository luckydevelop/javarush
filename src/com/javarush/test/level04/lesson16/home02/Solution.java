package com.javarush.test.level04.lesson16.home02;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них.
Т.е. не самое большое и не самое маленькое.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        String n1 = br.readLine();
        String n2 = br.readLine();
        String n3 = br.readLine();

        int m1 = Integer.parseInt(n1);
        int m2 = Integer.parseInt(n2);
        int m3 = Integer.parseInt(n3);

        if(m1>=m2&&m1<=m3||m1<=m2&&m1>=m3)
            System.out.print(m1);
        else if(m2>=m1&&m2<=m3||m2>=m3&&m2<=m1)
            System.out.print(m2);
        else
            System.out.print(m3);





    }
}
