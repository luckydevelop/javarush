package com.javarush.test.level04.lesson16.home03;

/* Посчитать сумму чисел
Вводить с клавиатуры числа и считать их сумму.
Если пользователь ввел -1, вывести на экран сумму и завершить программу.
-1 должно учитываться в сумме.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        boolean i = true;
        int sum = 0;

        while(i)
        {
            String n1 = br.readLine();
            int m1 = Integer.parseInt(n1);
            sum = sum + m1;
            if(m1==-1)
            {
                System.out.println(sum);
                break;
            }

        }
    }
}
