package com.javarush.test.level05.lesson07.task02;



public class Cat
{
    //Напишите тут ваш код

    String name;
    String color, adress;
    int ves, age;

    public void initialize(int ves, String color, String adress)
{
    this.color=color;
    this.adress=adress;
    this.age = 5;
    this.ves = 3;
}

    public void initialize(int ves, String color)
    {
        this.color=color;
        this.age = 5;
        this.ves = 3;
    }

    public void initialize(String name, int age)
    {
        this.color="green";
        this.name=name;
        this.age = age;
        this.ves = 3;
    }

    public void initialize(String name, int age, int ves)
    {
        this.color="green";
        this.name=name;
        this.age = age;
        this.ves = ves;
    }

    public void initialize(String name)
    {
        this.color="green";
        this.name=name;
        this.age = 5;
        this.ves = 3;
    }

}
/* Создать класс Cat
Создать класс Cat (кот) с пятью инициализаторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст неизвестны, это бездомный кот)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес неизвестен, то нужно указать какой-нибудь средний вес.
 Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/