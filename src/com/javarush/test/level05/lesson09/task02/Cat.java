package com.javarush.test.level05.lesson09.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью конструкторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст – неизвестные. Кот - бездомный)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес не известен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    String name;
    String color, adress;
    int ves, age;

    public Cat (int ves, String color, String adress)
    {
        this.color=color;
        this.adress=adress;
        this.age = 5;
        this.ves = 3;
    }

    public Cat (int ves, String color)
    {
        this.color=color;
        this.age = 5;
        this.ves = 3;
    }

    public Cat (String name, int age)
    {
        this.color="green";
        this.name=name;
        this.age = age;
        this.ves = 3;
    }

    public Cat (String name, int age, int ves)
    {
        this.color="green";
        this.name=name;
        this.age = age;
        this.ves = ves;
    }

    public Cat (String name)
    {
        this.color="green";
        this.name=name;
        this.age = 5;
        this.ves = 3;
    }


}
