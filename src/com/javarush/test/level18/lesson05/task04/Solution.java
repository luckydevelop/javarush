package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки ввода-вывода
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

//public class Solution {
//    public static void main(String[] args) throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
////        FileInputStream in = new FileInputStream(new File(reader.readLine()));
////        FileOutputStream out = new FileOutputStream(new File(reader.readLine()));
//        String fileName1 = "c://3.txt";
//        String fileName2 = "c://4.txt";
//        FileInputStream in = new FileInputStream(new File(fileName1));
//        FileOutputStream out = new FileOutputStream(new File(fileName2));
//
//        while(in.available() > 0){
//            byte[] res = new byte[in.available()];
//            int count = in.read(res);
//
//            for(int i = 0; i < res.length / 2; i++){
//                byte tmp = res[i];
//                res[i] = res[res.length-i-1];
//                res[res.length-i-1] = tmp;
//            }
//
//            out.write(res, 0, count);
//        }
//
//        FileInputStream fis2 = new FileInputStream(fileName2);
//        while(fis2.available()>0)
//        {
//            System.out.print(fis2.read());
//        }
//
//        reader.close();
//        in.close();
//        out.close();
//
//    }
//}

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
//        String fileName1 = "c://3.txt";
//        String fileName2 = "c://4.txt";

        FileInputStream fis1 = new FileInputStream(fileName1);
        FileOutputStream fos = new FileOutputStream(fileName2);

        List<Integer> lists = new ArrayList<Integer>();

        while(fis1.available()>0)
        {
            lists.add(fis1.read());
        }

        for(int i = lists.size()-1; i>=0; i--)
        {
            fos.write(lists.get(i));
        }

//        FileInputStream fis2 = new FileInputStream(fileName2);
//        while(fis2.available()>0)
//        {
//            System.out.println(fis2.read());
//        }

        br.close();
        fis1.close();
//        fis2.close();
        fos.close();

    }
}
