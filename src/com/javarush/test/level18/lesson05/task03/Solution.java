package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки ввода-вывода
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        String fileName3 = br.readLine();

        FileInputStream fis = new FileInputStream(fileName1);
        FileOutputStream fos1 = new FileOutputStream(fileName2);
        FileOutputStream fos2 = new FileOutputStream(fileName3);

        //List<Integer> lists = new ArrayList<Integer>();

        int fileSize = fis.available();

//        while(fis.available()>0)
//        {
//            lists.add(fis.read());
//        }

        //int fileSize = lists.size();
        int n = fileSize/2;
        byte[] buffer1 = new byte[fileSize-n];
        byte[] buffer2 = new byte[n];



        int count1 = fis.read(buffer1);
        int count2 = fis.read(buffer2);

        fos1.write(buffer1);
        fos2.write(buffer2);

        br.close();
        fos1.close();
        fos2.close();




    }
}


//public class Solution {
//    public static void main(String[] args) throws IOException
//    {
//
//
//    }
//}
