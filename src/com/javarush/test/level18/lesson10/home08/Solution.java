package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Не забудьте закрыть все потоки
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileNames;

        while(!(fileNames = br.readLine()).equals("exit"))
        {
            new ReadThread(fileNames).start();
        }
        br.close();
    }

    public static class ReadThread extends Thread {
        String fileName;
        int maxByte = 0;
        //List<Integer> list2 = new ArrayList<Integer>();


        public ReadThread(String fileName) throws FileNotFoundException
        {
            //implement constructor body
            this.fileName = fileName;

        }
        // implement file reading here - реализуйте чтение из файла тут


        @Override
        public void run()
        {
            try
            {
                FileInputStream fis = new FileInputStream(this.fileName);
                Map<Integer, Integer> res = new HashMap<Integer, Integer>();

                while (fis.available()>0)
                {
                    Integer key = fis.read();
                    Integer value = res.get(key);
                    res.put(key, (value==null)? 1 : (value+1) );
                }

                for( Map.Entry pair : res.entrySet())
                {
                    if(maxByte<(Integer)pair.getValue()) maxByte=(Integer)pair.getValue();

                }

                for( Map.Entry pair : res.entrySet())
                {
                    if((Integer)pair.getValue()==maxByte)
                        synchronized (resultMap)
                        {
                            resultMap.put(this.fileName, (Integer) pair.getKey());
                        }

                }


                fis.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }






        }


    }


}
