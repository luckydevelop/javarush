package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать содержимое третьего файла
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/


import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        String fileName3 = br.readLine();

        FileInputStream fis2 = new FileInputStream(fileName2);
        FileInputStream fis3 = new FileInputStream(fileName3);
        FileOutputStream fos1 = new FileOutputStream(fileName1);
        //byte[] buffer = new byte[];

        while(fis2.available()>0)
        {
            //int data = fis2.read();
            fos1.write(fis2.read());
        }

        while(fis3.available()>0)
        {
            //int data = fis2.read();
            fos1.write(fis3.read());
        }

        fis2.close();
        fis3.close();
        fos1.close();
        br.close();



    }
}

//public class Solution {
//    public static void main(String[] args) {
//
//
//    }
//}
