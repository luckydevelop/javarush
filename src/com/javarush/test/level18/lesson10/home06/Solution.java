package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки

Пример вывода:
, 19
- 7
f 361
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        String fileName = args[0];
        FileInputStream fis = new FileInputStream(fileName);
        //FileInputStream fis = new FileInputStream("c://1.txt");
        //List<Integer> list = new ArrayList<Integer>();
        Map<Character, Integer> map = new TreeMap<Character, Integer>();


        while(fis.available()>0)
        {
            char ch = (char)fis.read();
            if(map.containsKey(ch))
            {
                map.put(ch, map.get(ch)+1);
            }
            else map.put(ch, 1);

        }

for(Map.Entry<Character, Integer> pair : map.entrySet())
    System.out.println(pair.getKey()+ " " + pair.getValue());


    }
}
