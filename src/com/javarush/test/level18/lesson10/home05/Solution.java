package com.javarush.test.level18.lesson10.home05;

/* Округление чисел
Считать с консоли 2 имени файла
Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415
Округлить числа до целых и записать во второй файл
Закрыть потоки
Принцип округления:
3.49 - 3
3.50 - 4
3.51 - 4
*/

//import java.io.*;
//import java.util.Arrays;
//
//public class Solution {
//    public static void main(String[] args) throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//
//        File file1 = new File(reader.readLine());
//        File file2 = new File(reader.readLine());
//
//        FileInputStream in = new FileInputStream(file1);
//        FileOutputStream out = new FileOutputStream(file2);
//
//
//        while(in.available() > 0){
//            byte[] data = new byte[in.available()];
//            in.read(data);
//            out.write(getInput(data));
//        }
//
//
//        in.close();
//        out.close();
//
//    }
//
//    private static byte[] getInput(byte[] data){
//
//        StringBuilder builder = new StringBuilder();
//        String[] s = new String(data).split(" ");
//        byte[] res;
//        long[] tmp = new long[s.length];
//
//        for(int i = 0; i < s.length; i++){
//            tmp[i] = Math.round(Double.valueOf(s[i]));
//        }
//
//        for(long i : tmp){
//            builder.append(i);
//            builder.append(" ");
//        }
//
//        res = builder.toString().getBytes();
//
//        return res;
//    }
//}


import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String fileName1 = br.readLine();
        String fileName2 = br.readLine();

        FileInputStream fis = new FileInputStream(fileName1);
        FileOutputStream fos = new FileOutputStream(fileName2);

        byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        fos.write(getInput(buffer));

        fis.close();
        fos.close();
        br.close();


    }


        private static byte[] getInput (byte[] buf)
        {
            String[] s = new String(buf).split(" ");
            long [] l = new long[s.length];
            for(int i=0; i<s.length; i++) l[i] = Math.round(Double.parseDouble(s[i])) ;
            byte res[] = new byte[l.length];

            StringBuilder sb = new StringBuilder();

            for(int i=0; i<l.length; i++)
            {
                sb.append(l[i]);
                sb.append(" ");
            }

            res = sb.toString().getBytes();

            return res;
        }






}
