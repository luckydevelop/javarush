package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String fileName1 = br.readLine();
        String fileName2 = br.readLine();

//        FileInputStream fis1 = new FileInputStream(fileName1);
//        FileInputStream fis2 = new FileInputStream(fileName2);

        RandomAccessFile random1 = new RandomAccessFile(fileName1, "rw");
        RandomAccessFile random2 = new RandomAccessFile(fileName2, "r");

        byte[] buffer1 = new byte[(int)random1.length()];
        byte[] buffer2 = new byte[(int)random2.length()];

        random1.read(buffer1);
        random2.read(buffer2);

        //byte[] buffer12 = new byte[buffer1.length+buffer2.length];

        random1.seek(0);

        random1.write(buffer2);
        random1.write(buffer1);

        br.close();
        random1.close();
        random2.close();






    }
}
