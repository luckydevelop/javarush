package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран частоту встречания пробела. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
Закрыть потоки
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        FileInputStream fis = new FileInputStream(args[0]);
        int countAll=0;
        int countSpace=0;
        float howOften=0;
        //double howOften=0;

        while(fis.available()>0)
        {
            int data = fis.read();
            countAll++;
            if(data== Integer.valueOf(' ')) countSpace++;
        }

        howOften = (float)countSpace*100/countAll;

        System.out.printf("%.2f", howOften);
        fis.close();
    }

}


//public class Solution {
//    public static void main(String[] args) {
//
//    }
//
//}