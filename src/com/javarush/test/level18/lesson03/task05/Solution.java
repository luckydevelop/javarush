package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        //String fileName = "c://1.txt";
        FileInputStream fis = new FileInputStream(fileName);
        List<Integer> lists = new ArrayList<Integer>();

        while(fis.available()>0)
        {
            Integer bt = fis.read();
            if(!lists.contains(bt)) lists.add(bt);
        }

        Collections.sort(lists);

        for(Integer list : lists)
        System.out.print(list + " ");

        br.close();
        fis.close();

    }
}
