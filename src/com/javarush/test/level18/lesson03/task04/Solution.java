package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байты, которые встречаются в файле меньше всего раз.
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader readerFileName = new BufferedReader(new InputStreamReader(System.in));

        //String fileName = readerFileName.readLine();
        FileInputStream readerFileByte = new FileInputStream("c://1.txt");
        //FileInputStream readerFileByte = new FileInputStream(fileName);
        List<Integer> listsByte = new ArrayList<Integer>();

        while(readerFileByte.available()>0)
        {
            listsByte.add(readerFileByte.read());
        }

        Map<Integer, Integer> listValue = new HashMap<Integer, Integer>();


        for(Integer list1 : listsByte)
        {
            int count = 0;
            for(Integer list2 : listsByte)
            {
                if(list1==list2) count++;
            }

            if(!listValue.containsKey(list1)) listValue.put(list1, count);
        }

        int minValue = Integer.MAX_VALUE;
        Integer value;

        for(Map.Entry pair : listValue.entrySet())
        {
            //System.out.println(pair.getKey() +" "+ pair.getValue())  ;
            value = (Integer)pair.getValue();
            if(value<minValue) minValue = value;
        }

        String s="";

        for(Map.Entry pair : listValue.entrySet())
        {
            //System.out.println(pair.getKey() +" "+ pair.getValue())  ;
            value = (Integer)pair.getValue();
            if(value==minValue) s+=pair.getKey() + " ";
        }

        System.out.println(s);

        readerFileByte.close();
        readerFileByte.close();

    }
}
