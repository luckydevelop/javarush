package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые частые байты
Ввести с консоли имя файла
Найти байты, которые чаше всех встречаются в файле
Вывести их на экран через пробел.
Закрыть поток ввода-вывода
*/

//public class Solution {
//    //Инициализируем штучки
//    public static void main(String[] args) throws Exception {
//        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
//        FileInputStream inp = new FileInputStream(r.readLine());
//        String res="";
//        ArrayList<Integer> list = new ArrayList<Integer>();
//        ArrayList<Integer> reslist = new ArrayList<Integer>();
////пихаем байты в массив
//        while(inp.available()>0)
//        {
//            int data = inp.read();
//            list.add(data);
//        }
////в count считаем кол-во каждого байта
//        int[] count = new int[list.size()];
//        for (int i=0; i<list.size(); i++)
//        {
//            count[i] = 0;
//            for (int j = 0; j < list.size();j++)
//            {
//                if (list.get(i)==list.get(j)) count[i]++;
//            }
//        }
////определяем минимум
//        int max = Integer.MIN_VALUE;
//        for (int h: count)
//        {
//            if (h>max) max = h;
//        }
////собираем байты с минимумом использования и без повторов
//        for (int i = 0; i<list.size(); i++)
//        {
//            if (!reslist.contains(list.get(i))&&count[i]==max) reslist.add(list.get(i));
//        }
//        for (int y: reslist)
//        {
//            res = y + " ";
//        }
//        System.out.println(res);
//        r.close();
//        inp.close();
//
//    }
//}

//public class Solution
//{
//    public static void main(String[] args) throws Exception
//    {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//
//        String fileName = reader.readLine();
//        InputStream input = new FileInputStream(fileName);
//
//        List<Integer> list = new ArrayList<>();
//
//        while (input.available() > 0)
//        {
//            list.add(input.read());
//        }
//
//        Collections.sort(list);
//        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
//        for (int i : list)
//        {
//            int counter = 0;
//            for(int k = 0; k < list.size(); k++){
//                if(list.get(k).equals(i)){
//                    counter++;
//                }
//            }
//            map.put(i, counter);
//        }
//        List<Integer> valueList = new ArrayList<Integer>();
//
//        Iterator it = map.entrySet().iterator();
//        for(Map.Entry pair : map.entrySet()){
//            valueList.add(pair.getValue());
//        }
//
//        Collections.sort(valueList);
//
//        int maxFrequence = valueList.get(valueList.size()-1);
//
//        for(Map.Entry pair : map.entrySet()){
//            if(pair.getValue().equals(maxFrequence)){
//                System.out.print(pair.getKey() + " ");
//            }
//        }



//    }
//}
//public class Solution {
//    public static void main(String[] args) throws Exception {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName = reader.readLine();
//
//        FileInputStream fis = new FileInputStream(fileName);
//        List<Integer> lists = new ArrayList<Integer>();
//
//        while(fis.available()>0)
//        {
//            lists.add(fis.read());
//        }
//
//        int mustOftenNumber=0;
//        int nRaz=0;
//        int nRazMax=0;
//
//        for(Integer list1 : lists)
//        {
//            for(Integer list2 : lists)
//            {
//                if(list1==list2) nRaz++;
//            }
//            if (nRaz>nRazMax)
//            {
//                nRazMax = nRaz;
//                mustOftenNumber = list1;
//            }
//        }
//    }
//}


//public class Solution {
//    public static void main(String[] args) throws Exception {
//    }
//}
