package com.javarush.test.level19.lesson05.task05;

/* Пунктуация
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Удалить все знаки пунктуации, вывести во второй файл.
http://ru.wikipedia.org/wiki/%D0%9F%D1%83%D0%BD%D0%BA%D1%82%D1%83%D0%B0%D1%86%D0%B8%D1%8F
Закрыть потоки ввода-вывода.
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        BufferedReader reader = new BufferedReader(new FileReader(fileName1));

        FileWriter writer = new FileWriter(fileName2);

        while(reader.ready())
        {
           String s1 = reader.readLine();
            String s2 = s1.replaceAll( "\\p{Punct}","");
            writer.write(s2);
        }

        br.close();
        reader.close();
        writer.close();


    }
}


//public class Solution {
//    public static void main(String[] args) {
//    }
//}
