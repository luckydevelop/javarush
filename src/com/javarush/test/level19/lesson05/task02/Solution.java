package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть поток ввода.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
//
//public class Solution {
//    public static void main(String[] args) throws IOException
//    {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader readfile = new BufferedReader(new FileReader(reader.readLine()));
//        int count = 0;
//        while (readfile.ready())
//        {
//            String[] s = readfile.readLine().replaceAll("\\p{Punct}"," ").split(" ");
//            for (String u : s)
//            {
//                if ("world".equals(u)) count++;
//            }
//        }
//        System.out.println(count);
//    }
//}

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //String fileName = br.readLine();
        String fileName = "C://1.txt";
        FileReader fr = new FileReader(fileName);
        BufferedReader fileReader = new BufferedReader(fr);
        int count = 0;

        while(fileReader.ready())
        {
           String string = fileReader.readLine();
            String string2 = string.replaceAll("[\\p{Punct}]", " ");

            String[] string3 = string2.split(" ");


            for(String s : string3 )
            {
                if(s.equals("world")) count++;
            }
        }

        System.out.println(count);
        br.close();
        fileReader.close();

    }
}

