package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки ввода-вывода.

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       //String fileName1 = br.readLine();
        String fileName1 = "c://1.txt";
       // String fileName2 = br.readLine();
        String fileName2 = "c://2.txt";
        BufferedReader reader = new BufferedReader(new FileReader(fileName1));
        FileWriter writer = new FileWriter(fileName2);


        while(reader.ready())
        {
            String s1 = reader.readLine();
            String s2 = s1.replaceAll("[\\p{Punct}]", " ");
            String[] sArray = s2.split(" ");
            for(String s : sArray)
            {
                System.out.println(s);

            }

            for(String s : sArray)
            {
                if(isDigit(s))
                {
                    String sTemp = s + " ";
                    writer.write(sTemp);
                }
            }


        }

        br.close();
        reader.close();
        writer.close();

    }

    public static boolean isDigit(String s)
    {
        try
        {
            Integer.parseInt(s);
            return true;
        }

        catch (NumberFormatException e)
        {
            return false;
        }


    }
}


//public class Solution {
//    public static void main(String[] args) {
//    }
//}
