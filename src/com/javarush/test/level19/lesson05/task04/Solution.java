package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки ввода-вывода.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        FileReader reader = new FileReader(fileName1);
        FileWriter writer = new FileWriter(fileName2);

        while(reader.ready())
        {
          char ch =(char)reader.read();
            if(ch=='.') ch = '!';
            writer.write(ch);
        }

        reader.close();
        writer.close();
        br.close();


    }
}


//public class Solution {
//    public static void main(String[] args) {
//    }
//}
