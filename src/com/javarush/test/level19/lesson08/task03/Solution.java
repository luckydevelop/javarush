package com.javarush.test.level19.lesson08.task03;

/* Выводим только цифры
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна выводить только цифры
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток

Пример вывода:
12345678
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args)
    {
        PrintStream console = System.out;
        ByteArrayOutputStream newout = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(newout);
        System.setOut(printStream);
        testString.printSomething();
        System.setOut(console);
        String g = newout.toString().replaceAll("[\\p{Alpha}\\p{Punct}\\p{Space}]","");
        System.out.println(g);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's 1 a 23 text 4 f5-6or7 tes8ting");
        }
    }
}

//public class Solution {
//    public static TestString testString = new TestString();
//
//    public static void main(String[] args)
//    {
//        PrintStream consoleStream = System.out;
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        PrintStream stream = new PrintStream(outputStream);
//        System.setOut(stream);
//        testString.printSomething();
//        System.setOut(consoleStream);
//        String s1 = outputStream.toString();
//        String s2 = s1.replaceAll("[\\p{Alpha}\\p{Punct}\\p{Blank}]","");
//        System.out.println(s2);
//    }
//
//    public static class TestString {
//        public void printSomething() {
//            System.out.println("it's 1 a 23 text 4 f5-6or7 tes8ting");
//        }
//    }
//}
