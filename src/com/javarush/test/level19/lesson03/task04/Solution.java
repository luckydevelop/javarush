package com.javarush.test.level19.lesson03.task04;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/* И еще один адаптер
Адаптировать Scanner к PersonScanner.
Классом-адаптером является PersonScannerAdapter.
Данные в файле хранятся в следующем виде:
Иванов Иван Иванович 31 12 1978

Подсказка: воспользуйтесь классом Calendar
*/

public class Solution {
    public static class PersonScannerAdapter implements PersonScanner
    {

        Scanner scanner;

        PersonScannerAdapter(Scanner scanner)
        {
            this.scanner = scanner;
        }

        @Override
        public Person read() throws IOException
        {
            String line = scanner.nextLine();
            String[] sarray = line.split(" ");

            String firstName = sarray[1];
            String middleName = sarray[2];
            String lastName = sarray[0];
            int date = Integer.parseInt(sarray[3]);
            int month = Integer.parseInt(sarray[4])-1;
            int year = Integer.parseInt(sarray[5]);
            Calendar calendar = new GregorianCalendar(year, month, date);


            return new Person(firstName, middleName, lastName, calendar.getTime() );
        }
        //Иванов Иван Иванович 31 12 1978
       // public Person(String firstName, String middleName, String lastName, Date birthDate)
        @Override
        public void close() throws IOException
        {
            scanner.close();
        }
    }
}


//public class Solution {
//    public static class PersonScannerAdapter {
//
//    }
//}
