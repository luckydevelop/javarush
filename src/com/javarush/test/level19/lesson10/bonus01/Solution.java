package com.javarush.test.level19.lesson10.bonus01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Отслеживаем изменения
Считать в консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME
Пример:
[Файл 1]
строка1
строка2
строка3

[Файл 2]
строка1
строка3
строка4

[Результат - список lines]
SAME строка1
REMOVED строка2
SAME строка3
ADDED строка4
*/



//public class Solution {
//    public static List<LineItem> lines = new ArrayList<LineItem>();
//
//    public static void main(String[] args) throws IOException
//    {
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String fileName1 = br.readLine();
//        String fileName2 = br.readLine();
//        BufferedReader reader1 = new BufferedReader(new FileReader(fileName1));
//        BufferedReader reader2 = new BufferedReader(new FileReader(fileName2));
//        List<String> list1 = new ArrayList<String>();
//        List<String> list2 = new ArrayList<String>();
//
//        while(reader1.ready())
//        {
//            list1.add(reader1.readLine());
//        }
//
//        while(reader2.ready())
//        {
//            list2.add(reader2.readLine());
//        }
//
//        for(int i=0; i<list1.size(); i++)
//        {
//            if(list) lines.add(new LineItem(Type.SAME, list1.get(i)));
//            else new LineItem(Type.REMOVED, list1.get(i)
//        }
//
//    }
//    //else if(list1.get(i).equals(list2.get(i+1))) new LineItem(Type.ADDED, list2.get(i) ) new LineItem(Type.SAME, list1.get(i) );
//
//    public static enum Type {
//        ADDED,        //добавлена новая строка
//        REMOVED,      //удалена строка
//        SAME          //без изменений
//    }
//
//    public static class LineItem {
//        public Type type;
//        public String line;
//
//        public LineItem(Type type, String line) {
//            this.type = type;
//            this.line = line;
//        }
//    }
//}
