package com.javarush.test.level19.lesson10.home03;

import java.io.*;
import java.util.*;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException
    {
        String fileName = args[0];
        BufferedReader br = new BufferedReader(new FileReader(fileName));

        while(br.ready())
        {
           String line = br.readLine();
            String[] array = line.split(" ");
            String name = array[0];

            if(array.length>4)
            {
                for (int i = 1; i < 3; i++)
                {
                    if(!isDigit(array[i])) name = name + " " + array[i];
                }
            }

            Calendar calendar = new GregorianCalendar(Integer.parseInt(array[array.length-1]), (Integer.parseInt(array[array.length-2]))-1, Integer.parseInt(array[array.length-3])  );
            Date date = calendar.getTime();
            Person person = new Person(name, date);

            PEOPLE.add(person);
            System.out.println(name);
        }

        br.close();

    }

    static boolean isDigit(String s)
    {
        try
        {
            Integer.parseInt(s);
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

}

//public class Solution {
//    public static final List<Person> PEOPLE = new ArrayList<Person>();
//
//    public static void main(String[] args)
//    {
//
//    }
//
//}