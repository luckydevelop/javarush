package com.javarush.test.level19.lesson10.bonus02;

/* Свой FileWriter
Реализовать логику FileConsoleWriter
Должен наследоваться от FileWriter
При записи данных в файл, должен дублировать эти данные на консоль
*/


import java.io.File;
        import java.io.FileDescriptor;
        import java.io.FileWriter;
        import java.io.IOException;

public class FileConsoleWriter extends FileWriter{
    public FileConsoleWriter(String f)  throws IOException
    {
        super(f);
    }
    public FileConsoleWriter(String f, boolean a)  throws IOException
    {
        super(f, a);
    }
    public FileConsoleWriter(File f)  throws IOException
    {
        super(f);
    }
    public FileConsoleWriter(File f, boolean a)  throws IOException
    {
        super(f, a);
    }
    public FileConsoleWriter(FileDescriptor f)  throws IOException
    {
        super(f);
    }

    @Override
    public void write(int c) throws IOException
    {
        super.write(c);
        System.out.print((char)c);

    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException
    {
        super.write(cbuf, off, len);
        System.out.print(String.valueOf(cbuf, off, len));
    }

    @Override
    public void write(String str, int off, int len) throws IOException
    {

        super.write(str, off, len);
        System.out.print(str.substring(off,off+len));
    }

    @Override
    public void write(char[] cbuf) throws IOException
    {
        super.write(cbuf);
        System.out.print(String.valueOf(cbuf));

    }

    @Override
    public void write(String str) throws IOException
    {
        super.write(str);
        System.out.print(str);
    }
//    public static void main(String[] args)
//    {
//        try {
//            FileConsoleWriter consoleWriter = new FileConsoleWriter("test.txt");
//            consoleWriter.write("blia");
//            consoleWriter.close();
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    public static void main(String[] args) throws IOException
    {
        FileConsoleWriter fileConsoleWriter = new FileConsoleWriter("C:\\1.txt");

        fileConsoleWriter.write("�������� String:");

        fileConsoleWriter.write(1234);

        char[] buff = "�������� char buff:".toCharArray();

        fileConsoleWriter.write(buff);

        fileConsoleWriter.write(buff, 3, 5);

        fileConsoleWriter.write("�������� String �������:", 1, 6);

        fileConsoleWriter.flush();
        fileConsoleWriter.close();
    }

}