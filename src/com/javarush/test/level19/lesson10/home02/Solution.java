package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        String fileName = args[0];
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        Map<String, Double> treeMap = new HashMap<String, Double>();

        while(br.ready())
        {
            String line = br.readLine();
            String[] array = line.split(" ");
            Double value = Double.parseDouble(array[1]);

            if(treeMap.containsKey(array[0]))
            {
                treeMap.put(array[0], treeMap.get(array[0])+value);
            }
            else
            treeMap.put(array[0], value);

//            if(treeMap.containsKey(array[0])) value = value+treeMap.get(array[0]);
//            treeMap.put(array[0], value);
        }

        for(Map.Entry<String, Double> pair  : treeMap.entrySet() )
        {
            System.out.println(pair.getKey()+ " " + pair.getValue());
        }

        Double max = 0D;


        for(Map.Entry<String, Double> pair  : treeMap.entrySet() )
        {
            if(pair.getValue()>max)
            {
                max = pair.getValue();
            }
        }

        for(Map.Entry<String, Double> pair  : treeMap.entrySet() )
        {
            if(pair.getValue()==max)
            {
                System.out.println(pair.getKey());
            }
        }

        for(Map.Entry<String, Double> pair  : treeMap.entrySet() )
        {
                System.out.println(pair.getKey()+ " " + pair.getValue());
        }



    }
}
