package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит слова, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
       String fileIn = args[0];
       String fileOut = args[1];

        BufferedReader br = new BufferedReader(new FileReader(fileIn));
        FileWriter fw = new FileWriter(fileOut);

        while(br.ready())
        {
            String string1 = br.readLine();
            //String string2 = string1.replaceAll("[\\p{Punct}]", " ");
            String[] array = string1.split(" ");

            for(String s:array)
            {
                char[] array2 = s.toCharArray();

                for(char s2 : array2)
                {
                    if (Character.isDigit(s2))
                    {
                        fw.write(s + " ");
                        break;
                    }
                }

            }



        }

        br.close();
        fw.close();
    }


}

//public class Solution {
//    public static void main(String[] args) {
//    }
//}
