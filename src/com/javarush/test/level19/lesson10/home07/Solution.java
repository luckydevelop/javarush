package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки

Пример выходных данных:
длинное,короткое,аббревиатура
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        String fileName1 = args[0];
        //String fileName1 = "c://2.txt";
        String fileName2 = args[1];
        //String fileName2 = "c://3.txt";
        BufferedReader br = new BufferedReader(new FileReader(fileName1));
        FileWriter fw = new FileWriter(fileName2);
        List<String> list = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();

        while(br.ready())
        {
            String s1 = br.readLine();
            String[] array = s1.split(" ");
            for(int i=0; i<array.length; i++)
            {
                if(array[i].length()>6)
                {
                    list.add(array[i]);
                }
            }
        }

        for(int i=0; i<list.size()-1; i++)
        {
            sb.append(list.get(i)+",");
        }

        sb.append(list.get(list.size()-1));
        String res = sb.toString();
        fw.write(res);

        br.close();
        fw.close();


    }
}

//public class Solution {
//    public static Map<Integer, String> map = new HashMap<Integer, String>();
//
//    public static void main(String[] args) {
//
//    }
//}