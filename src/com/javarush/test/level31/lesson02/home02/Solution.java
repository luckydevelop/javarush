package com.javarush.test.level31.lesson02.home02;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/* Находим все файлы
Реализовать логику метода getFileTree, который должен в директории root найти список всех файлов включая вложенные.
Используйте очередь, рекурсию не используйте.
Верните список всех путей к найденным файлам, путь к директориям возвращать не надо.
Путь должен быть абсолютный.
*/
public class Solution {

    public static void main(String[] args) throws IOException
    {
        List<String> list = getFileTree("E:\\Downloads\\Torrents\\Avast");

        for (String s : list)
        {
            System.out.println(s);
        }

    }

    public static List<String> getFileTree(String root) throws IOException
    {
        File rootFile = new File(root);
        Queue<File> queue = new ArrayDeque<File>();
        queue.add(rootFile);
        List<String> list = new ArrayList<String>();

        while(!queue.isEmpty())
        {
          File x = queue.poll();
          File[] array = x.listFiles();

            for (File file : array)
            {
                if(file.isFile()) list.add(file.getAbsolutePath());
                else queue.add(file);
            }


        }

        return list;

    }
}
