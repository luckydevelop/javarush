package com.javarush.test.level31.lesson02.home01;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.util.*;

/* Проход по дереву файлов
1. На вход метода main подаются два параметра.
Первый - path - путь к директории, второй - resultFileAbsolutePath - имя файла, который будет содержать результат.
2. Для каждого файла в директории path и в ее всех вложенных поддиректориях выполнить следующее:
2.1. Если у файла длина в байтах больше 50, то удалить его.
2.2. Если у файла длина в байтах НЕ больше 50, то для всех таких файлов:
2.2.1. отсортировать их по имени файла в возрастающем порядке, путь не учитывать при сортировке
2.2.2. переименовать resultFileAbsolutePath в 'allFilesContent.txt'
2.2.3. в allFilesContent.txt последовательно записать содержимое всех файлов из п. 2.2.1. Тела файлов разделять "\n"
2.3. Удалить директории без файлов (пустые).
Все файлы имеют расширение txt.
*/
public class Solution {
    public static void main(String[] args) throws IOException
    {
        File path = new File(args[0]);
        File resultFileAbsolutePath = new File(args[1]);
        File newFileName = new File(resultFileAbsolutePath.getParent()+File.separator+"allFilesContent.txt");

        List<File> finalListFile = getListFile(path, resultFileAbsolutePath);

        delete50(finalListFile);

        finalListFile = getListFile(path, resultFileAbsolutePath);



        Collections.sort(finalListFile, new Comparator<File>()
        {
            @Override
            public int compare(File o1, File o2)
            {
                return o1.getName().compareTo(o2.getName());
            }
        });



     boolean res = resultFileAbsolutePath.renameTo(newFileName);
      System.out.println(res);
        //resultFileAbsolutePath = newFileName;
        //System.out.println(resultFileAbsolutePath);

       writeToFile(finalListFile, newFileName);

        delEmptyDir(path);
    }

    public static List<File> getListFile(File file, File resultFileAbsolutePath)
    {
        File[] arrayFile = file.listFiles();
        List<File> listFile = new ArrayList<File>(Arrays.asList(arrayFile));
        List<File> tempList = new ArrayList<File>(Arrays.asList(arrayFile));
        for (File temp : tempList)
        {
            if(temp.getAbsolutePath().equals(resultFileAbsolutePath.getAbsolutePath()))
            {
                listFile.remove(temp);
            }

            if(temp.isDirectory())
            {
                listFile.remove(temp);
                listFile.addAll(getListFile(temp, resultFileAbsolutePath));
            }
        }




        return listFile;
    }


    public static void delEmptyDir (File file)
    {
        File[] arrayFile = file.listFiles();
        List<File> listFile = new ArrayList<File>(Arrays.asList(arrayFile));

        for (File f : listFile)
        {
            if(f.isDirectory())
            {
               boolean b = f.delete();
               if(!b) delEmptyDir(f);
            }
        }
    }


    public static void writeToFile(List<File> list, File newFileName) throws IOException
    {
        FileOutputStream fos = new FileOutputStream(newFileName);

        for (File file : list)
        {
            FileInputStream fis = new FileInputStream(file);

            while(fis.available()>0)
            {
                int b = fis.read();
                fos.write(b);
            }

            int n = list.indexOf(file);
            if(n<list.size()-1)
            {
                fos.write('\n');
            }
            fis.close();
            fos.flush();
        }
        fos.close();


    }



    public static void delete50 (List <File> list)
    {
        for(File f : list)
        {
            if (f.length()>50) f.delete();
        }

    }



}
