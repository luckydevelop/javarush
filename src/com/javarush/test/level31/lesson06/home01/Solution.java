package com.javarush.test.level31.lesson06.home01;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* Добавление файла в архив
В метод main приходит список аргументов.
Первый аргумент - полный путь к файлу fileName.
Второй аргумент - путь к zip-архиву.
Добавить файл (fileName) внутрь архива в директорию 'new'.
Если в архиве есть файл с таким именем, то заменить его.

Пример входных данных:
C:/result.mp3
C:/pathToTest/test.zip

Файлы внутри test.zip:
a.txt
b.txt

После запуска Solution.main архив test.zip должен иметь такое содержимое:
new/result.mp3
a.txt
b.txt

Подсказка: нужно сначала куда-то сохранить содержимое всех энтри,
а потом записать в архив все энтри вместе с добавленным файлом.
Пользоваться файловой системой нельзя.
*/



public class Solution
{

// OK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//    public static void main(String... args) throws IOException
//{

//        String source="E:/Downloads/Torrents/Avast/allFilesContent.txt";
//        File sfile= new File(source);
//
//        String dest="E:/Downloads/Torrents/Avast/test2.zip";
//        File dfile= new File(dest);
//
//        FileInputStream fis= new FileInputStream(sfile);
//        FileOutputStream fos= new FileOutputStream(dfile);
//
//        ZipOutputStream zos= new ZipOutputStream(fos);
//        ZipEntry ze= new ZipEntry(source);
//
//        //begins writing a new zip file and sets the the position to the start of data
//        zos.putNextEntry(ze);
//
//        byte[] buf = new byte[1024];
//        int len;
//
//        while((len=fis.read(buf))>0)
//        {
//            zos.write(buf, 0, len);
//        }
//
//        System.out.println("File created:"+dest);
//        fis.close();
//        zos.close();
//    }


// NOT OKKK!!!!!!!!!!!!!!!1
    public static void main(String... args) throws IOException
    {
/*        String source = "E:/Downloads/Torrents/Avast/allFilesContent.txt";
        File sfile = new File (source);

        String dest = "E:/Downloads/Torrents/Avast/test2.zip";
        File dfile = new File(dest);

        FileInputStream fis = new FileInputStream(sfile);
        FileOutputStream fos = new FileOutputStream(dfile);

        ZipOutputStream zos = new ZipOutputStream(fos);
        ZipEntry ze = new ZipEntry(source);

        zos.putNextEntry(ze);
        byte[] buf =  new byte[1024];
        int len;*/


        String source="E:/Downloads/Torrents/Avast/allFilesContent.txt";
        File sfile= new File(source);

        String dest="E:/Downloads/Torrents/Avast/test2.zip";
        File dfile= new File(dest);

        FileInputStream fis= new FileInputStream(sfile);
        FileOutputStream fos= new FileOutputStream(dfile);

        ZipOutputStream zos= new ZipOutputStream(fos);
        ZipEntry ze= new ZipEntry(source);

        //begins writing a new zip file and sets the the position to the start of data
        zos.putNextEntry(ze);

        byte[] buf = new byte[1024];
        int len;


 /*       while((len=fis.read(buf))>0)
        {
           zos.write(buf, 0, len);
        }*/

                while((len=fis.read(buf))>0)
        {
            zos.write(buf, 0, len);
        }

        /////////// old
//        System.out.println("File created:"+dest);
//
//        fis.close();
//        fos.close();

                System.out.println("File created:"+dest);
        fis.close();
        zos.close();
    }

}

