package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

/**
 * Created by Lucky on 18.01.2015.
 */
public class ImageReaderFactory
{
    public static ImageReader getReader (ImageTypes imgType) // тут проверить
    {
        ImageReader imgReader=null;

        if(imgType==ImageTypes.BMP) imgReader = new BmpReader();
        else if(imgType==ImageTypes.PNG) imgReader = new PngReader();
        else if(imgType==ImageTypes.JPG) imgReader = new JpgReader();
        else throw new IllegalArgumentException("Неизвестный тип картинки");


        return imgReader;
    }
}
